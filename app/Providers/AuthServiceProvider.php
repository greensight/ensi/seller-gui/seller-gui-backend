<?php

namespace App\Providers;

use App\Domain\Auth\UserProviders\TokenUserProvider;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     */
    protected $policies = [
      // 'App\Model' => 'App\Policies\ModelPolicy',
   ];

    /**
     * Register any authentication / authorization services.
     */
    public function boot(): void
    {
        //
    }

    public function register(): void
    {
        parent::register();

        $this->app->make('auth')->provider('ensi_token', function ($app) {
            return $app->make(TokenUserProvider::class);
        });
    }
}
