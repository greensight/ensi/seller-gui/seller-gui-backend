<?php

namespace App\Http\ApiV1\Modules\Common\Controllers;

use App\Domain\Common\Actions\GetMenuItemsForUserAction;
use App\Http\ApiV1\Support\Resources\DataResource;
use Illuminate\Contracts\Support\Responsable;

class MenuController
{
    public function menu(GetMenuItemsForUserAction $action): Responsable
    {
        return new DataResource($action->execute());
    }
}
