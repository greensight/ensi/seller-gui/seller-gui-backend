<?php

namespace App\Http\ApiV1\Modules\Units\Resources\Stores;

use App\Http\ApiV1\Modules\Units\Resources\AddressResource;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\BuClient\Dto\Store;

/** @mixin Store */
class StoresResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'seller_id' => $this->getSellerId(),
            'xml_id' => $this->getXmlId(),
            'active' => $this->getActive(),
            'name' => $this->getName(),
            'address' => AddressResource::make($this->whenNotNull($this->getAddress())),
            'timezone' => $this->getTimezone(),
            'created_at' => $this->dateTimeToIso($this->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->getUpdatedAt()),

            'workings' => StoreWorkingsResource::collection($this->whenNotNull($this->getWorkings())),
            'contacts' => StoreContactsResource::collection($this->whenNotNull($this->getContacts())),
            'contact' => new StoreContactsResource($this->whenNotNull($this->getContact())),
            'pickup_times' => StorePickupTimesResource::collection($this->whenNotNull($this->getPickupTimes())),
        ];
    }
}
