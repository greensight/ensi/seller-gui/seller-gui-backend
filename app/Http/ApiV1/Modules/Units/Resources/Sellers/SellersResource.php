<?php

namespace App\Http\ApiV1\Modules\Units\Resources\Sellers;

use App\Http\ApiV1\Modules\Units\Resources\AddressResource;
use App\Http\ApiV1\Modules\Units\Resources\Stores\StoresResource;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\BuClient\Dto\Seller;

/** @mixin Seller */
class SellersResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'manager_id' => $this->getManagerId(),
            'legal_name' => $this->getLegalName(),
            'legal_address' => AddressResource::make($this->whenNotNull($this->getLegalAddress())),
            'fact_address' => AddressResource::make($this->whenNotNull($this->getFactAddress())),
            'inn' => $this->getInn(),
            'kpp' => $this->getKpp(),
            'payment_account' => $this->getPaymentAccount(),
            'correspondent_account' => $this->getCorrespondentAccount(),
            'bank' => $this->getBank(),
            'bank_address' => AddressResource::make($this->whenNotNull($this->getBankAddress())),
            'bank_bik' => $this->getBankBik(),
            'status' => $this->getStatus(),
            'status_at' => $this->dateTimeToIso($this->getStatusAt()),
            'site' => $this->getSite(),
            'info' => $this->getInfo(),
            'created_at' => $this->dateTimeToIso($this->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->getUpdatedAt()),

            'stores' => StoresResource::collection($this->whenNotNull($this->getStores())),
        ];
    }
}
