<?php

namespace App\Http\ApiV1\Modules\Units\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\BuClient\Dto\Address;

/** @mixin Address */
class AddressResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            "address_string" => $this->getAddressString(),
            "country_code" => $this->getCountryCode(),
            "post_index" => $this->getPostIndex(),
            "region" => $this->getRegion(),
            "region_guid" => $this->getRegionGuid(),
            "area" => $this->getArea(),
            "area_guid" => $this->getAreaGuid(),
            "city" => $this->getCity(),
            "city_guid" => $this->getCityGuid(),
            "street" => $this->getStreet(),
            "house" => $this->getHouse(),
            "block" => $this->getBlock(),
            "flat" => $this->getFlat(),
            "floor" => $this->getFloor(),
            "porch" => $this->getPorch(),
            "intercom" => $this->getIntercom(),
            "geo_lat" => $this->getGeoLat(),
            "geo_lon" => $this->getGeoLon(),
        ];
    }
}
