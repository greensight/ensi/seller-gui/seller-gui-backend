<?php

namespace App\Http\ApiV1\Modules\Units\Resources\SellerUsers;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\SellerAuthClient\Dto\User;

/**
 * @mixin User
 */
class SellerUsersResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            "id" => $this->getId(),
            "seller_id" => $this->getSellerId(),
            "login" => $this->getLogin(),
            "active" => $this->getActive(),
            "first_name" => $this->getFirstName(),
            "last_name" => $this->getLastName(),
            "middle_name" => $this->getMiddleName(),
            "email" => $this->getEmail(),
            "phone" => $this->getPhone(),

            "created_at" => $this->dateTimeToIso($this->getCreatedAt()),
            "updated_at" => $this->dateTimeToIso($this->getUpdatedAt()),
        ];
    }
}
