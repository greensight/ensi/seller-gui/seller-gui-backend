<?php

namespace App\Http\ApiV1\Modules\Units\Controllers\SellerUsers;

use App\Domain\Common\Data\Meta\Field;
use App\Domain\Units\Actions\SellerUsers\CreateSellerUserAction;
use App\Domain\Units\Actions\SellerUsers\GetSellerUserAction;
use App\Domain\Units\Actions\SellerUsers\PatchSellerUserAction;
use App\Domain\Units\Actions\SellerUsers\SearchSellerUsersAction;
use App\Http\ApiV1\Modules\Units\Requests\SellerUsers\CreateSellerUserRequest;
use App\Http\ApiV1\Modules\Units\Requests\SellerUsers\PatchSellerUserRequest;
use App\Http\ApiV1\Modules\Units\Resources\SellerUsers\SellerUsersResource;
use App\Http\ApiV1\Support\Requests\SearchRequest;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Illuminate\Contracts\Support\Responsable;

class SellerUsersController
{
    public function create(CreateSellerUserRequest $request, CreateSellerUserAction $action): Responsable
    {
        return SellerUsersResource::make($action->execute($request->validated()));
    }

    public function patch(int $id, PatchSellerUserRequest $request, PatchSellerUserAction $action): Responsable
    {
        return SellerUsersResource::make($action->execute($id, $request->validated()));
    }

    public function get(int $id, SearchRequest $request, GetSellerUserAction $action): Responsable
    {
        return SellerUsersResource::make($action->execute($id, $request));
    }

    public function search(SearchRequest $request, SearchSellerUsersAction $action): Responsable
    {
        return SellerUsersResource::collectPage($action->execute($request));
    }

    public function meta(): ModelMetaResource
    {
        return new ModelMetaResource([
            Field::id()->listDefault()->filterDefault()->sortDefault(direction: 'desc')->detailLink(),
            Field::boolean('active', 'Активность')->listDefault()->filterDefault(),
            Field::text('last_name', 'Фамилия')->listDefault()->filterDefault()->resetSort(),
            Field::text('first_name', 'Имя')->listDefault()->filterDefault()->resetSort(),
            Field::text('middle_name', 'Отчество')->listDefault()->filterDefault()->resetSort(),
            Field::phone('phone', 'Телефон')->listDefault()->filterDefault()->resetSort(),
            Field::text('email', 'Email')->listDefault()->filterDefault()->resetSort(),
            Field::datetime('created_at', 'Дата создания')->listDefault()->filterDefault(),
            Field::datetime('updated_at', 'Дата обновления')->listDefault()->filterDefault(),
        ]);
    }
}
