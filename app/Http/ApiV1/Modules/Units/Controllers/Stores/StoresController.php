<?php

namespace App\Http\ApiV1\Modules\Units\Controllers\Stores;

use App\Domain\Common\Data\Meta\Field;
use App\Domain\Units\Actions\Stores\Stores\CreateStoreAction;
use App\Domain\Units\Actions\Stores\Stores\GetStoreAction;
use App\Domain\Units\Actions\Stores\Stores\PatchStoreAction;
use App\Domain\Units\Actions\Stores\Stores\SearchStoresAction;
use App\Http\ApiV1\Modules\Units\Requests\Stores\CreateStoreRequest;
use App\Http\ApiV1\Modules\Units\Requests\Stores\PatchStoreRequest;
use App\Http\ApiV1\Modules\Units\Resources\Stores\StoresResource;
use App\Http\ApiV1\Support\Requests\SearchRequest;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Illuminate\Contracts\Support\Responsable;

class StoresController
{
    public function create(CreateStoreRequest $request, CreateStoreAction $action): Responsable
    {
        return StoresResource::make($action->execute($request->validated()));
    }

    public function patch(int $id, PatchStoreRequest $request, PatchStoreAction $action): Responsable
    {
        return StoresResource::make($action->execute($id, $request->validated()));
    }

    public function get(int $id, SearchRequest $request, GetStoreAction $action): Responsable
    {
        return StoresResource::make($action->execute($id, $request));
    }

    public function search(SearchRequest $request, SearchStoresAction $action): Responsable
    {
        return StoresResource::collectPage($action->execute($request));
    }

    public function meta(): Responsable
    {
        return new ModelMetaResource([
            Field::id()->listDefault()->filterDefault()->detailLink(),
            Field::text('name', 'Название')->listDefault()->filterDefault()->sort(),
            Field::string('address.address_string', 'Полный адрес')->object()->filter('address_string'),
            Field::boolean('active', 'Активность')->listDefault()->filterDefault()->sort(),
            Field::datetime('created_at', 'Дата создания')->listDefault(),
            Field::datetime('updated_at', 'Дата обновления')->listDefault(),
        ]);
    }
}
