<?php

namespace App\Http\ApiV1\Modules\Units\Controllers\Stores;

use App\Domain\Units\Actions\Stores\StorePickupTimes\CreateStorePickupTimeAction;
use App\Domain\Units\Actions\Stores\StorePickupTimes\DeleteStorePickupTimeAction;
use App\Domain\Units\Actions\Stores\StorePickupTimes\PatchStorePickupTimeAction;
use App\Http\ApiV1\Modules\Units\Requests\Stores\CreateStorePickupTimeRequest;
use App\Http\ApiV1\Modules\Units\Requests\Stores\PatchStorePickupTimeRequest;
use App\Http\ApiV1\Modules\Units\Resources\Stores\StorePickupTimesResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class StorePickupTimesController
{
    public function create(CreateStorePickupTimeRequest $request, CreateStorePickupTimeAction $action): Responsable
    {
        return StorePickupTimesResource::make($action->execute($request->validated()));
    }

    public function patch(int $id, PatchStorePickupTimeRequest $request, PatchStorePickupTimeAction $action): Responsable
    {
        return StorePickupTimesResource::make($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteStorePickupTimeAction $action): Responsable
    {
        $action->execute($id);

        return new EmptyResource();
    }
}
