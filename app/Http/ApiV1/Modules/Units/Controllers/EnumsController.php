<?php

namespace App\Http\ApiV1\Modules\Units\Controllers;

use App\Domain\Common\Data\EnumData;
use App\Http\ApiV1\Support\Resources\EnumResource;
use Ensi\BuClient\Dto\SellerStatusEnum;
use Illuminate\Contracts\Support\Responsable;

class EnumsController
{
    public function sellerStatuses(): Responsable
    {
        return EnumResource::collection(EnumData::makeFromEnumDescriptions(SellerStatusEnum::getDescriptions()));
    }
}
