<?php

namespace App\Http\ApiV1\Modules\Units\Controllers\Sellers;

use App\Domain\Units\Actions\Sellers\GetSellerAction;
use App\Domain\Units\Actions\Sellers\PatchSellerAction;
use App\Http\ApiV1\Modules\Units\Requests\Sellers\PatchSellerRequest;
use App\Http\ApiV1\Modules\Units\Resources\Sellers\SellersResource;
use App\Http\ApiV1\Support\Requests\SearchRequest;
use Illuminate\Contracts\Support\Responsable;

class SellersController
{
    public function get(GetSellerAction $action, SearchRequest $request): Responsable
    {
        return SellersResource::make($action->execute($request));
    }

    public function patch(PatchSellerRequest $request, PatchSellerAction $action): Responsable
    {
        return SellersResource::make($action->execute($request->validated()));
    }
}
