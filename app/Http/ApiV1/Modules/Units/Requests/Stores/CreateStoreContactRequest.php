<?php

namespace App\Http\ApiV1\Modules\Units\Requests\Stores;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class CreateStoreContactRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'store_id' => ['required', 'integer'],
            'name' => ['nullable', 'string'],
            'phone' => ['nullable', 'string'],
            'email' => ['nullable', 'string'],
        ];
    }
}
