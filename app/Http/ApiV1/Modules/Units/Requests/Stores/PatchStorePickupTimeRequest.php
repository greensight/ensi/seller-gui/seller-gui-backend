<?php

namespace App\Http\ApiV1\Modules\Units\Requests\Stores;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class PatchStorePickupTimeRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'store_id' => ['sometimes', 'integer'],
            'day' => ['sometimes', 'integer'],
            'pickup_time_code' => ['nullable', 'string'],
            'pickup_time_start' => ['sometimes', 'string'],
            'pickup_time_end' => ['sometimes', 'string'],
            'delivery_service' => ['nullable', 'integer'],
        ];
    }
}
