<?php

namespace App\Http\ApiV1\Modules\Units\Requests\Stores;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class CreateStorePickupTimeRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'store_id' => ['required', 'integer'],
            'day' => ['required', 'integer'],
            'pickup_time_code' => ['nullable', 'string'],
            'pickup_time_start' => ['required', 'string'],
            'pickup_time_end' => ['required', 'string'],
            'delivery_service' => ['nullable', 'integer'],
        ];
    }
}
