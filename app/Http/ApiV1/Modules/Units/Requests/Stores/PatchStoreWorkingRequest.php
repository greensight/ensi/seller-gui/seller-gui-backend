<?php

namespace App\Http\ApiV1\Modules\Units\Requests\Stores;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class PatchStoreWorkingRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'store_id' => ['sometimes', 'integer'],
            'active' => ['sometimes', 'bool'],
            'day' => ['sometimes', 'int'],
            'working_start_time' => ['nullable', 'string'],
            'working_end_time' => ['nullable', 'string'],
        ];
    }
}
