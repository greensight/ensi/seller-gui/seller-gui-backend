<?php

namespace App\Http\ApiV1\Modules\Units\Requests\Stores;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class CreateStoreWorkingRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'store_id' => ['required', 'integer'],
            'active' => ['required', 'bool'],
            'day' => ['required', 'int'],
            'working_start_time' => ['nullable', 'string'],
            'working_end_time' => ['nullable', 'string'],
        ];
    }
}
