<?php

namespace App\Http\ApiV1\Modules\Units\Requests\Stores;

use App\Http\ApiV1\Modules\Units\Requests\AddressRequest;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class PatchStoreRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'xml_id' => ['nullable', 'string'],
            'active' => ['sometimes', 'boolean'],
            'name' => ['sometimes', 'string'],
            ...self::nestedRules('address', AddressRequest::baseRules()),
            'timezone' => ['nullable', 'string'],
        ];
    }
}
