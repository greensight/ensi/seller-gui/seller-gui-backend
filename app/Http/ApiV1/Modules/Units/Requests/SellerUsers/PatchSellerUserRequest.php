<?php

namespace App\Http\ApiV1\Modules\Units\Requests\SellerUsers;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class PatchSellerUserRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'login' => ['sometimes', 'string'],
            'password' => ['sometimes', 'string'],
            'active' => ['sometimes', 'boolean'],
            'first_name' => ['sometimes', 'string'],
            'last_name' => ['sometimes', 'string'],
            'middle_name' => ['sometimes', 'string'],
            "phone" => ['sometimes', 'regex:/^\+7\d{10}$/'],
            'email' => ['sometimes', 'string'],
        ];
    }
}
