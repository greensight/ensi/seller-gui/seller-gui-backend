<?php

namespace App\Http\ApiV1\Modules\Units\Requests\Sellers;

use App\Http\ApiV1\Modules\Units\Requests\AddressRequest;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class PatchSellerRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'legal_name' => ['sometimes', 'string'],
            ...self::nestedRules('legal_address', AddressRequest::baseRules()),
            ...self::nestedRules('fact_address', AddressRequest::baseRules()),
            'inn' => ['nullable', 'string'],
            'kpp' => ['nullable', 'string'],
            'payment_account' => ['nullable', 'string'],
            'correspondent_account' => ['nullable', 'string'],
            'bank' => ['nullable', 'string'],
            ...self::nestedRules('bank_address', AddressRequest::baseRules()),
            'bank_bik' => ['nullable', 'string'],
            'site' => ['nullable', 'string'],
            'info' => ['nullable', 'string'],
        ];
    }
}
