<?php

namespace App\Http\ApiV1\Modules\Units\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class AddressRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return self::baseRules();
    }

    public static function baseRules(): array
    {
        return [
            "address_string" => ['nullable', 'string'],
            "country_code" => ['nullable', 'string'],
            "post_index" => ['nullable', 'string'],
            "region" => ['nullable', 'string'],
            "region_guid" => ['nullable', 'string'],
            "area" => ['nullable', 'string'],
            "area_guid" => ['nullable', 'string'],
            "city" => ['nullable', 'string'],
            "city_guid" => ['nullable', 'string'],
            "street" => ['nullable', 'string'],
            "house" => ['nullable', 'string'],
            "block" => ['nullable', 'string'],
            "flat" => ['nullable', 'string'],
            "floor" => ['nullable', 'string'],
            "porch" => ['nullable', 'string'],
            "intercom" => ['nullable', 'string'],
            "geo_lat" => ['nullable', 'string'],
            "geo_lon" => ['nullable', 'string'],
        ];
    }
}
