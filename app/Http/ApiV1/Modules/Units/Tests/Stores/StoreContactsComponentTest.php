<?php

use App\Domain\Auth\Models\User;
use App\Domain\Units\Tests\Factories\StoreContactFactory;
use App\Http\ApiV1\Modules\Units\Tests\Stores\Factories\StoreContactRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\BuClient\Dto\CreateStoreContactRequest;
use Ensi\BuClient\Dto\EmptyDataResponse;
use Ensi\BuClient\Dto\ExtendedPatchStoreContactRequest;
use Ensi\BuClient\Dto\FilterStoreContactsRequest;

use function Pest\Laravel\deleteJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;
use function PHPUnit\Framework\assertEquals;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'units', 'stores');

test('POST /api/v1/units/stores-contacts 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $sellerId = 1;
    $this->authorize(User::factory()->withSeller($sellerId)->make());

    $request = StoreContactRequestFactory::new()->make();
    $apiRequest = null;

    $this->mockBusinessUnitsStoreContactsApi()
        ->shouldReceive('createStoreContact')
        ->once()
        ->withArgs(function (CreateStoreContactRequest $request) use (&$apiRequest) {
            $apiRequest = $request;

            return true;
        })
        ->andReturn(StoreContactFactory::new()->makeResponseOne());

    postJson('/api/v1/units/stores-contacts', $request)
        ->assertStatus(200);

    assertEquals($sellerId, $apiRequest->getSellerId());
});

test('DELETE /api/v1/units/stores-contacts/{id} 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $sellerId = 1;
    $this->authorize(User::factory()->withSeller($sellerId)->make());

    $apiRequest = null;

    $this->mockBusinessUnitsStoreContactsApi()
        ->shouldReceive('deleteStoreContact')
        ->once()
        ->withArgs(function (int $id, FilterStoreContactsRequest $request) use (&$apiRequest) {
            $apiRequest = $request;

            return true;
        })
        ->andReturn(new EmptyDataResponse());

    deleteJson('/api/v1/units/stores-contacts/1')
        ->assertStatus(200);

    assertEquals($sellerId, $apiRequest->getFilter()->seller_id);
});

test('PATCH /api/v1/units/stores-contacts/{id} 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $sellerId = 1;
    $this->authorize(User::factory()->withSeller($sellerId)->make());

    $storeContactId = 1;

    $this->mockBusinessUnitsStoreContactsApi()
        ->shouldReceive('extendedPatchStoreContact')
        ->once()
        ->withArgs(function (int $id, ExtendedPatchStoreContactRequest $request) use ($storeContactId, &$apiRequest) {
            $apiRequest = $request;

            return $id === $storeContactId;
        })
        ->andReturn(StoreContactFactory::new()->makeResponseOne(['id' => $storeContactId]));

    $request = StoreContactRequestFactory::new()->make();

    patchJson("/api/v1/units/stores-contacts/$storeContactId", $request)
        ->assertStatus(200);

    assertEquals($sellerId, $apiRequest->getFilter()->seller_id);
});
