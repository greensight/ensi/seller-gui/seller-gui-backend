<?php

namespace App\Http\ApiV1\Modules\Units\Tests\Stores\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;

class StoreWorkingRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'store_id' => $this->faker->modelId(),
            'active' => $this->faker->boolean,
            'day' => $this->faker->numberBetween(1, 7),
            'working_start_time' => $this->faker->time('H:i'),
            'working_end_time' => $this->faker->time('H:i'),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
