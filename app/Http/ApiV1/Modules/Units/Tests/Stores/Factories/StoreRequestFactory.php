<?php

namespace App\Http\ApiV1\Modules\Units\Tests\Stores\Factories;

use App\Http\ApiV1\Support\Tests\Factories\BaseAddressFactory;
use Ensi\LaravelTestFactories\BaseApiFactory;

class StoreRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'seller_id' => $this->faker->nullable()->modelId(),
            'xml_id' => $this->faker->word(),
            'active' => $this->faker->boolean(),
            'name' => $this->faker->name(),
            'address' => BaseAddressFactory::new()->make(),
            'timezone' => $this->faker->timezone(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
