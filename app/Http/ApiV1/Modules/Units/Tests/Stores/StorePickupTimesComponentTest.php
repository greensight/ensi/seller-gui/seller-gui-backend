<?php

use App\Domain\Auth\Models\User;
use App\Domain\Units\Tests\Factories\StorePickupTimeFactory;
use App\Http\ApiV1\Modules\Units\Tests\Stores\Factories\StorePickupTimeRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\BuClient\Dto\CreateStorePickupTimeRequest;
use Ensi\BuClient\Dto\EmptyDataResponse;
use Ensi\BuClient\Dto\ExtendedPatchStorePickupTimeRequest;
use Ensi\BuClient\Dto\FilterStorePickupTimesRequest;

use function Pest\Laravel\deleteJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;
use function PHPUnit\Framework\assertEquals;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'units', 'stores');

test('POST /api/v1/units/stores-pickup-times 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $sellerId = 1;
    $this->authorize(User::factory()->withSeller($sellerId)->make());

    $request = StorePickupTimeRequestFactory::new()->make();

    $apiRequest = null;
    $this->mockBusinessUnitsStorePickupTimesApi()
        ->shouldReceive('createStorePickupTime')
        ->once()
        ->withArgs(function (CreateStorePickupTimeRequest $request) use (&$apiRequest) {
            $apiRequest = $request;

            return true;
        })
        ->andReturn(StorePickupTimeFactory::new()->makeResponseOne());

    postJson('/api/v1/units/stores-pickup-times', $request)
        ->assertStatus(200);

    assertEquals($sellerId, $apiRequest->getSellerId());
});

test('DELETE /api/v1/units/stores-pickup-times/{id} 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $sellerId = 1;
    $this->authorize(User::factory()->withSeller($sellerId)->make());

    $apiRequest = null;

    $this->mockBusinessUnitsStorePickupTimesApi()
        ->shouldReceive('deleteStorePickupTime')
        ->once()
        ->withArgs(function (int $id, FilterStorePickupTimesRequest $request) use (&$apiRequest) {
            $apiRequest = $request;

            return true;
        })
        ->andReturn(new EmptyDataResponse());

    deleteJson('/api/v1/units/stores-pickup-times/1')
        ->assertStatus(200);

    assertEquals($sellerId, $apiRequest->getFilter()->seller_id);
});

test('PATCH /api/v1/units/stores-pickup-times/{id} 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $sellerId = 1;
    $this->authorize(User::factory()->withSeller($sellerId)->make());

    $storePickupTimeId = 1;

    $this->mockBusinessUnitsStorePickupTimesApi()
        ->shouldReceive('extendedPatchStorePickupTime')
        ->once()
        ->withArgs(function (int $id, ExtendedPatchStorePickupTimeRequest $request) use ($storePickupTimeId, &$apiRequest) {
            $apiRequest = $request;

            return $id === $storePickupTimeId;
        })
        ->andReturn(StorePickupTimeFactory::new()->makeResponseOne(['id' => $storePickupTimeId]));

    $request = StorePickupTimeRequestFactory::new()->make();

    patchJson("/api/v1/units/stores-pickup-times/$storePickupTimeId", $request)
        ->assertStatus(200);

    assertEquals($sellerId, $apiRequest->getFilter()->seller_id);
});
