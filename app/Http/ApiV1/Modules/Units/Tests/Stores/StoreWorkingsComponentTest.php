<?php

use App\Domain\Auth\Models\User;
use App\Domain\Units\Tests\Factories\StoreWorkingFactory;
use App\Http\ApiV1\Modules\Units\Tests\Stores\Factories\StoreWorkingRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\BuClient\Dto\CreateStoreWorkingRequest;
use Ensi\BuClient\Dto\EmptyDataResponse;
use Ensi\BuClient\Dto\ExtendedPatchStoreWorkingRequest;
use Ensi\BuClient\Dto\FilterStoreWorkingsRequest;

use function Pest\Laravel\deleteJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;
use function PHPUnit\Framework\assertEquals;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'units', 'stores');

test('POST /api/v1/units/stores-workings 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $sellerId = 1;
    $this->authorize(User::factory()->withSeller($sellerId)->make());

    $request = StoreWorkingRequestFactory::new()->make();
    $apiRequest = null;

    $this->mockBusinessUnitsStoreWorkingsApi()
        ->shouldReceive('createStoreWorking')
        ->once()
        ->withArgs(function (CreateStoreWorkingRequest $request) use (&$apiRequest) {
            $apiRequest = $request;

            return true;
        })
        ->andReturn(StoreWorkingFactory::new()->makeResponseOne());

    postJson('/api/v1/units/stores-workings', $request)
        ->assertStatus(200);

    assertEquals($sellerId, $apiRequest->getSellerId());
});

test('DELETE /api/v1/units/stores-workings/{id} 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $sellerId = 1;
    $this->authorize(User::factory()->withSeller($sellerId)->make());

    $apiRequest = null;

    $this->mockBusinessUnitsStoreWorkingsApi()
        ->shouldReceive('deleteStoreWorking')
        ->once()
        ->withArgs(function (int $id, FilterStoreWorkingsRequest $request) use (&$apiRequest) {
            $apiRequest = $request;

            return true;
        })
        ->andReturn(new EmptyDataResponse());

    deleteJson('/api/v1/units/stores-workings/1')
        ->assertStatus(200);

    assertEquals($sellerId, $apiRequest->getFilter()->seller_id);
});

test('PATCH /api/v1/units/stores-workings/{id} 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $sellerId = 1;
    $this->authorize(User::factory()->withSeller($sellerId)->make());

    $storeWorkingId = 1;

    $this->mockBusinessUnitsStoreWorkingsApi()
        ->shouldReceive('extendedPatchStoreWorking')
        ->once()
        ->withArgs(function (int $id, ExtendedPatchStoreWorkingRequest $request) use ($storeWorkingId, &$apiRequest) {
            $apiRequest = $request;

            return $id === $storeWorkingId;
        })
        ->andReturn(StoreWorkingFactory::new()->makeResponseOne(['id' => $storeWorkingId]));

    $request = StoreWorkingRequestFactory::new()->make();

    patchJson("/api/v1/units/stores-workings/$storeWorkingId", $request)
        ->assertStatus(200);

    assertEquals($sellerId, $apiRequest->getFilter()->seller_id);
});
