<?php

use App\Domain\Auth\Models\User;
use App\Domain\Units\Tests\Factories\StoreFactory;
use App\Http\ApiV1\Modules\Units\Tests\Stores\Factories\StoreRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\BuClient\Dto\CreateStoreRequest;
use Ensi\BuClient\Dto\ExtendedPatchStoreRequest;
use Ensi\BuClient\Dto\SearchStoresRequest;

use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;
use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertEqualsCanonicalizing;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'units', 'stores');

test('GET /api/v1/units/stores:meta 200', function () {
    /** @var ApiV1ComponentTestCase $this */

    $meta = getJson('/api/v1/units/stores:meta')
        ->assertStatus(200)
        ->json();

    assertMeta($meta);
});

test('POST /api/v1/units/stores:search 200', function (bool $includeAllRelations) {
    /** @var ApiV1ComponentTestCase $this */
    $sellerId = 1;
    $this->authorize(User::factory()->withSeller($sellerId)->make());

    $relations = ['workings', 'contacts', 'contact', 'pickup_times'];
    $count = 3;
    $apiRequest = null;

    $this->mockBusinessUnitsStoresApi()
        ->shouldReceive('searchStores')
        ->once()
        ->withArgs(function (SearchStoresRequest $request) use (&$apiRequest) {
            $apiRequest = $request;

            return true;
        })
        ->andReturn(
            StoreFactory::new()
                ->withAllRelations($includeAllRelations)
                ->makeResponseSearch(count: $count)
        );

    $request = [
        'include' => $includeAllRelations ? $relations : [],
    ];
    $response = postJson('/api/v1/units/stores:search', $request)
        ->assertStatus(200)
        ->assertJsonCount($count, 'data');

    if ($includeAllRelations) {
        $response->assertJsonStructure(['data' => [$relations]]);
        assertEqualsCanonicalizing($relations, $apiRequest->getInclude());
    }

    assertEquals($sellerId, $apiRequest->getFilter()->seller_id);
})->with([
    'without includes' => [false],
    'with all includes' => [true],
]);

test('POST /api/v1/units/stores 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $sellerId = 1;
    $this->authorize(User::factory()->withSeller($sellerId)->make());

    $request = StoreRequestFactory::new()->make();
    $apiRequest = null;

    $this->mockBusinessUnitsStoresApi()
        ->shouldReceive('createStore')
        ->once()
        ->withArgs(function (CreateStoreRequest $request) use (&$apiRequest) {
            $apiRequest = $request;

            return true;
        })
        ->andReturn(StoreFactory::new()->makeResponseOne());

    postJson('/api/v1/units/stores', $request)
        ->assertStatus(200);

    assertEquals($sellerId, $apiRequest->getSellerId());
});

test('GET /api/v1/units/stores/{id} 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $sellerId = 1;
    $this->authorize(User::factory()->withSeller($sellerId)->make());

    $id = 1;

    $apiRequest = null;
    $this->mockBusinessUnitsStoresApi()
        ->shouldReceive('searchOneStore')
        ->once()
        ->withArgs(function (SearchStoresRequest $request) use (&$apiRequest) {
            $apiRequest = $request;

            return true;
        })
        ->andReturn(StoreFactory::new()->makeResponseOne(['id' => $id]));

    getJson("/api/v1/units/stores/$id")
        ->assertStatus(200);

    assertEquals($sellerId, $apiRequest->getFilter()->seller_id);
    assertEquals($id, $apiRequest->getFilter()->id);
});

test('PATCH /api/v1/units/stores/{id} 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $sellerId = 1;
    $this->authorize(User::factory()->withSeller($sellerId)->make());

    $storeId = 1;

    $this->mockBusinessUnitsStoresApi()
        ->shouldReceive('extendedPatchStore')
        ->once()
        ->withArgs(function (int $id, ExtendedPatchStoreRequest $request) use ($storeId, &$apiRequest) {
            $apiRequest = $request;

            return $id === $storeId;
        })
        ->andReturn(StoreFactory::new()->makeResponseOne(['id' => $storeId]));

    $request = StoreRequestFactory::new()->make();

    patchJson("/api/v1/units/stores/$storeId", $request)
        ->assertStatus(200);

    assertEquals($sellerId, $apiRequest->getFilter()->seller_id);
});
