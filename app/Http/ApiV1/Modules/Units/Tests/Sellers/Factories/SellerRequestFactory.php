<?php

namespace App\Http\ApiV1\Modules\Units\Tests\Sellers\Factories;

use App\Http\ApiV1\Support\Tests\Factories\BaseAddressFactory;
use App\Http\ApiV1\Support\Tests\Factories\BaseRequestFactory;
use Ensi\BuClient\Dto\SellerStatusEnum;

class SellerRequestFactory extends BaseRequestFactory
{
    protected function definition(): array
    {
        return [
            'legal_name' => $this->faker->unique()->company(),
            'legal_address' => $this->faker->nullable()->exactly(BaseAddressFactory::new()->make()),
            'fact_address' => $this->faker->nullable()->exactly(BaseAddressFactory::new()->make()),
            'inn' => $this->faker->nullable()->numerify('##########'),
            'kpp' => $this->faker->nullable()->numerify('#########'),
            'payment_account' => $this->faker->nullable()->numerify('####################'),
            'correspondent_account' => $this->faker->nullable()->numerify('####################'),
            'bank' => $this->faker->nullable()->company(),
            'bank_address' => $this->faker->nullable()->exactly(BaseAddressFactory::new()->make()),
            'bank_bik' => $this->faker->nullable()->numerify('#########'),
            'status' => $this->faker->randomElement(SellerStatusEnum::getAllowableEnumValues()),
            'site' => $this->faker->nullable()->url(),
            'info' => $this->faker->nullable()->text(),
        ];
    }
}
