<?php

use App\Domain\Auth\Models\User;
use App\Domain\Units\Tests\Factories\SellerFactory;
use App\Http\ApiV1\Modules\Units\Tests\Sellers\Factories\SellerRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\BuClient\Dto\PatchSellerRequest;

use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function PHPUnit\Framework\assertEquals;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'units', 'sellers');


test('GET /api/v1/units/sellers include all 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $sellerId = 1;
    $this->authorize(User::factory()->withSeller($sellerId)->make());

    $storesCount = 2;
    $include = 'stores';

    $this->mockBusinessUnitsSellersApi()
        ->shouldReceive('getSeller')
        ->once()
        ->withArgs(function (int $id, string $includeRequest) use ($sellerId, $include) {
            assertEquals($sellerId, $id);
            assertEquals($include, $includeRequest);

            return true;
        })
        ->andReturn(
            SellerFactory::new()
                ->withStores($storesCount)
                ->makeResponse(['id' => $sellerId])
        );

    getJson("/api/v1/units/sellers?include={$include}")
        ->assertOk()
        ->assertJsonPath('data.id', $sellerId)
        ->assertJsonStructure(['data' => ['id', 'legal_name', 'status', 'stores']])
        ->assertJsonCount($storesCount, 'data.stores');
});

test('PATCH /api/v1/units/sellers 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $sellerId = 1;
    $this->authorize(User::factory()->withSeller($sellerId)->make());

    $this->mockBusinessUnitsSellersApi()
        ->shouldReceive('patchSeller')
        ->once()
        ->withArgs(fn (int $id, PatchSellerRequest $seller) => $id == $sellerId)
        ->andReturn(SellerFactory::new()->makeResponse(['id' => $sellerId]));

    $request = SellerRequestFactory::new()->make();

    patchJson("/api/v1/units/sellers", $request)
        ->assertOk()
        ->assertJsonPath('data.id', $sellerId)
        ->assertJsonStructure(['data' => ['id', 'legal_name', 'status']]);
});
