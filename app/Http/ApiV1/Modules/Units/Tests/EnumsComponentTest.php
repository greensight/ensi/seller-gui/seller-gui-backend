<?php

use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use Ensi\BuClient\Dto\SellerStatusEnum;

use function Pest\Laravel\getJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('GET /api/v1/units/sellers/seller-statuses 200', function () {
    /** @var ApiV1ComponentTestCase $this */

    $item = [
        'id' => SellerStatusEnum::ACTIVE,
        'name' => SellerStatusEnum::getDescriptions()[SellerStatusEnum::ACTIVE],
    ];

    getJson('/api/v1/units/sellers/seller-statuses')
        ->assertOk()
        ->assertJsonFragment($item);
});
