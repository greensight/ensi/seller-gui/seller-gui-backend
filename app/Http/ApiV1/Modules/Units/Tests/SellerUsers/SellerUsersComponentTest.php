<?php

use App\Domain\Auth\Models\User;
use App\Domain\Units\Tests\Factories\SellerUserFactory;
use App\Http\ApiV1\Modules\Units\Tests\SellerUsers\Factories\SellerUserRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelTestFactories\PaginationFactory;
use Ensi\SellerAuthClient\Dto\CreateUserRequest;

use Ensi\SellerAuthClient\Dto\ExtendedPatchUserRequest;
use Ensi\SellerAuthClient\Dto\SearchUsersRequest;

use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;
use function PHPUnit\Framework\assertEquals;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'units', 'seller-users');

test('POST /api/v1/units/seller-users 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $sellerId = 1;
    $this->authorize(User::factory()->withSeller($sellerId)->make());

    $apiRequest = null;
    $this->mockSellerAuthUsersApi()
        ->shouldReceive('createUser')
        ->once()
        ->withArgs(function (CreateUserRequest $request) use (&$apiRequest) {
            $apiRequest = $request;

            return true;
        })
        ->andReturn(SellerUserFactory::new()->makeResponse());

    $request = SellerUserRequestFactory::new()->make();

    postJson("/api/v1/units/seller-users", $request)
        ->assertStatus(200);

    assertEquals($sellerId, $apiRequest->getSellerId());
});

test('PATCH /api/v1/units/seller-users/{id} 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $sellerId = 1;
    $this->authorize(User::factory()->withSeller($sellerId)->make());

    $sellerUserId = 1;
    $apiRequest = null;
    $this->mockSellerAuthUsersApi()
        ->shouldReceive('extendedPatchUser')
        ->once()
        ->withArgs(function (int $id, ExtendedPatchUserRequest $request) use ($sellerUserId, &$apiRequest) {
            $apiRequest = $request;

            return $id === $sellerUserId;
        })
        ->andReturn(SellerUserFactory::new()->makeResponse(['id' => $sellerUserId]));

    $request = SellerUserRequestFactory::new()->make();

    patchJson("/api/v1/units/seller-users/{$sellerUserId}", $request)
        ->assertStatus(200)
        ->assertJsonPath('data.id', $sellerUserId)
        ->assertJsonStructure(['data' => ['id', 'login', 'email']]);

    assertEquals($sellerId, $apiRequest->getFilter()->seller_id);
});

test('GET /api/v1/units/seller-users/{id} 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $sellerId = 1;
    $this->authorize(User::factory()->withSeller($sellerId)->make());

    $sellerUserId = 1;
    $apiRequest = null;

    $this->mockSellerAuthUsersApi()
        ->shouldReceive('searchUser')
        ->once()
        ->withArgs(function (SearchUsersRequest $request) use (&$apiRequest) {
            $apiRequest = $request;

            return true;
        })
        ->andReturn(SellerUserFactory::new()->makeResponse(['id' => $sellerUserId]));

    getJson("/api/v1/units/seller-users/{$sellerUserId}")
        ->assertStatus(200)
        ->assertJsonStructure(['data' => ['id', 'login', 'email']]);

    assertEquals($sellerId, $apiRequest->getFilter()->seller_id);
    assertEquals($sellerUserId, $apiRequest->getFilter()->id);
});

test('POST /api/v1/units/seller-users:search 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $sellerId = 1;
    $this->authorize(User::factory()->withSeller($sellerId)->make());

    $sellerUsers = SellerUserFactory::new()->makeResponseSearch();
    $apiRequest = null;

    $this->mockSellerAuthUsersApi()
        ->shouldReceive('searchUsers')
        ->once()
        ->withArgs(function (SearchUsersRequest $request) use (&$apiRequest) {
            $apiRequest = $request;

            return true;
        })
        ->andReturn($sellerUsers);

    postJson("/api/v1/units/seller-users:search", ['pagination' => PaginationFactory::new()->makeRequestOffset()])
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', current($sellerUsers->getData())->getId());

    assertEquals($sellerId, $apiRequest->getFilter()->seller_id);
});

test('GET /api/v1/units/seller-users:meta success', function () {
    $meta = getJson('/api/v1/units/seller-users:meta')
        ->assertOk()
        ->json();

    assertMeta($meta);
});
