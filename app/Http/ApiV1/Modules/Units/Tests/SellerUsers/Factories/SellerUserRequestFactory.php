<?php

namespace App\Http\ApiV1\Modules\Units\Tests\SellerUsers\Factories;

use App\Http\ApiV1\Support\Tests\Factories\BaseRequestFactory;

class SellerUserRequestFactory extends BaseRequestFactory
{
    protected function definition(): array
    {
        return [
            'login' => $this->faker->unique()->userName(),
            'active' => $this->faker->boolean(),
            'password' => $this->faker->password(),
            'first_name' => $this->faker->firstName(),
            'last_name' => $this->faker->lastName(),
            'middle_name' => $this->faker->firstName(),
            'phone' => $this->faker->unique()->numerify('+7##########'),
            'email' => $this->faker->unique()->safeEmail(),
        ];
    }
}
