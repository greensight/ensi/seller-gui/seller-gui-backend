<?php

use App\Domain\Logistic\Tests\DeliveryServices\Factories\DeliveryServiceFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'logistic', 'logistic-delivery-services');

test("POST /api/v1/logistic/delivery-services:search success", function () {
    /** @var ApiV1ComponentTestCase $this */
    $deliveryServiceId = 1;

    $this->mockLogisticDeliveryServicesApi()->allows([
        'searchDeliveryServices' => DeliveryServiceFactory::new()->makeResponseSearch([['id' => $deliveryServiceId]]),
    ]);

    postJson("/api/v1/logistic/delivery-services:search")
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $deliveryServiceId);
});

test('POST /api/v1/logistic/delivery-service-enum-values:search 200', function ($key, $value) {
    /** @var ApiV1ComponentTestCase $this */
    $this->mockLogisticDeliveryServicesApi()->allows([
        'searchDeliveryServices' => DeliveryServiceFactory::new()->makeResponseSearch(),
    ]);

    postJson("/api/v1/logistic/delivery-service-enum-values:search", ['filter' => [$key => $value]])->assertStatus(200);
})->with([
    ['id', [1, 2]],
    ['query', 'name'],
]);
