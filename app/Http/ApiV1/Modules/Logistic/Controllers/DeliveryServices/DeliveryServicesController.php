<?php

namespace App\Http\ApiV1\Modules\Logistic\Controllers\DeliveryServices;

use App\Http\ApiV1\Modules\Logistic\Queries\DeliveryServices\DeliveryServicesQuery;
use App\Http\ApiV1\Modules\Logistic\Resources\DeliveryServices\DeliveryServiceEnumValueResource;
use App\Http\ApiV1\Modules\Logistic\Resources\DeliveryServices\DeliveryServicesResource;
use App\Http\ApiV1\Support\Requests\CommonFilterEnumValuesRequest;
use Illuminate\Contracts\Support\Responsable;

class DeliveryServicesController
{
    public function search(DeliveryServicesQuery $query): Responsable
    {
        return DeliveryServicesResource::collectPage($query->get());
    }

    public function searchEnumValues(CommonFilterEnumValuesRequest $request, DeliveryServicesQuery $query): Responsable
    {
        return DeliveryServiceEnumValueResource::collection($query->searchEnums());
    }
}
