<?php

namespace App\Http\ApiV1\Modules\Logistic\Queries\DeliveryServices;

use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderFilterEnumTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderFindTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderFirstTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderGetTrait;
use Ensi\LogisticClient\Api\DeliveryServicesApi;
use Ensi\LogisticClient\ApiException;
use Ensi\LogisticClient\Dto\DeliveryServiceResponse;
use Ensi\LogisticClient\Dto\RequestBodyPagination;
use Ensi\LogisticClient\Dto\SearchDeliveryServicesRequest;
use Ensi\LogisticClient\Dto\SearchDeliveryServicesResponse;
use Illuminate\Http\Request;

class DeliveryServicesQuery extends QueryBuilder
{
    use QueryBuilderFindTrait;
    use QueryBuilderFirstTrait;
    use QueryBuilderGetTrait;
    use QueryBuilderFilterEnumTrait;

    public function __construct(protected Request $httpRequest, protected DeliveryServicesApi $deliveryServicesApi)
    {
        parent::__construct($httpRequest);
    }

    protected function requestFirstClass(): string
    {
        return SearchDeliveryServicesRequest::class;
    }

    protected function requestGetClass(): string
    {
        return SearchDeliveryServicesRequest::class;
    }

    protected function paginationClass(): string
    {
        return RequestBodyPagination::class;
    }

    /**
     * @throws ApiException
     */
    public function searchById($id, string $include): DeliveryServiceResponse
    {
        return $this->deliveryServicesApi->getDeliveryService($id, $include);
    }

    /**
     * @throws ApiException
     */
    public function search($request): SearchDeliveryServicesResponse
    {
        return $this->deliveryServicesApi->searchDeliveryServices($request);
    }

    /**
     * @throws ApiException
     */
    public function searchOne($request): DeliveryServiceResponse
    {
        return $this->deliveryServicesApi->searchOneDeliveryService($request);
    }

    protected function prepareEnumRequest($request, ?array $id, ?string $query): void
    {
        $filter = [];
        if ($id) {
            $filter['id'] = $id;
        }
        if ($query) {
            $filter['name_like'] = $query;
        }
        $request->setFilter((object)$filter);
    }
}
