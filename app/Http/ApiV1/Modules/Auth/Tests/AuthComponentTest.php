<?php

use App\Domain\Auth\Models\Tests\Factories\TokenFactory;
use App\Domain\Common\Tests\Factories\JWTFactory;
use App\Domain\Units\Tests\Factories\SellerUserFactory;
use App\Http\ApiV1\Support\Tests\ApiV1NoAuthComponentTestCase;
use Ensi\SellerAuthClient\ApiException;

use function Pest\Laravel\getJson;
use function Pest\Laravel\postJson;

uses(ApiV1NoAuthComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/auth/login 200', function () {
    /** @var ApiV1NoAuthComponentTestCase $this */
    $token = TokenFactory::new()->make();

    $this->mockSellerAuthOauthApi()
        ->shouldReceive('createToken')
        ->once()
        ->andReturn($token);

    postJson("/api/v1/auth/login", [
        'login' => 'test_login',
        'password' => 'test_password',
    ])
        ->assertOk()
        ->assertJsonPath('data.access_token', $token->getAccessToken())
        ->assertJsonPath('data.refresh_token', $token->getRefreshToken())
        ->assertJsonPath('data.expires_in', $token->getExpiresIn());
});

test('POST /api/v1/auth/login 401', function () {
    /** @var ApiV1NoAuthComponentTestCase $this */

    $this->mockSellerAuthOauthApi()
        ->shouldReceive('createToken')
        ->once()
        ->andThrowExceptions([new ApiException("Unauthorized", 401)]);

    postJson("/api/v1/auth/login", [
        'login' => 'test_login',
        'password' => 'test_password',
    ])->assertUnauthorized();
});

test('POST /api/v1/auth/logout 200', function () {
    /** @var ApiV1NoAuthComponentTestCase $this */
    $this->mockSellerAuthUsersApi()
        ->shouldReceive('getCurrentUser')
        ->once()
        ->andReturn(SellerUserFactory::new()->makeResponse());

    $this->mockSellerAuthOauthApi()
        ->shouldReceive('deleteToken')
        ->once();

    getJson("/api/v1/auth/logout", [config('auth.header_for_token') => JWTFactory::new()->make()])
        ->assertOk()
        ->assertJsonPath('data', null);
});

test('POST /api/v1/auth/refresh 200', function () {
    /** @var ApiV1NoAuthComponentTestCase $this */
    $newToken = TokenFactory::new()->make();

    $this->mockSellerAuthOauthApi()
        ->shouldReceive('createToken')
        ->once()
        ->andReturn($newToken);

    postJson("/api/v1/auth/refresh", [
        'refresh_token' => JWTFactory::new()->make(),
    ])
        ->assertOk()
        ->assertJsonPath('data.access_token', $newToken->getAccessToken())
        ->assertJsonPath('data.refresh_token', $newToken->getRefreshToken())
        ->assertJsonPath('data.expires_in', $newToken->getExpiresIn());
});

test('POST /api/v1/auth/refresh 401', function () {
    /** @var ApiV1NoAuthComponentTestCase $this */

    $this->mockSellerAuthOauthApi()
        ->shouldReceive('createToken')
        ->once()
        ->andThrows(new ApiException("Unauthorized", 401));

    postJson("/api/v1/auth/refresh", [
        'refresh_token' => 'not_valid_token',
    ])
        ->assertUnauthorized();
});

test('POST /api/v1/auth/current-user 200', function () {
    /** @var ApiV1NoAuthComponentTestCase $this */
    $user = SellerUserFactory::new()->makeResponse();

    $this->mockSellerAuthUsersApi()
        ->shouldReceive('getCurrentUser')
        ->once()
        ->andReturn($user);

    getJson("/api/v1/auth/current-user", [config('auth.header_for_token') => JWTFactory::new()->make()])
        ->assertOk()
        ->assertJsonPath('data.id', $user->getData()->getId())
        ->assertJsonPath('data.login', $user->getData()->getLogin());
});

test('POST /api/v1/auth/current-user 401', function () {
    /** @var ApiV1NoAuthComponentTestCase $this */

    $this->mockSellerAuthUsersApi()
        ->shouldReceive('getCurrentUser')
        ->once()
        ->andThrows(new ApiException("Unauthorized", 401));

    getJson("/api/v1/auth/current-user", [config('auth.header_for_token') => 'Bearer not_valid_token'])
        ->assertUnauthorized();
});
