<?php

namespace App\Http\ApiV1\Modules\Auth\Resources;

use App\Domain\Auth\Models\User;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin User
 */
class CurrentUserResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            "id" => $this->id,
            "seller_id" => $this->sellerId,
            "login" => $this->login,
            "active" => $this->active,
            "full_name" => $this->fullName,
            "short_name" => $this->shortName,
            "first_name" => $this->firstName,
            "last_name" => $this->lastName,
            "middle_name" => $this->middleName,
            "phone" => $this->phone,
            "email" => $this->email,
        ];
    }
}
