<?php

namespace App\Http\ApiV1\Modules\Auth\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class RefreshRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'refresh_token' => ['required', 'string'],
        ];
    }

    public function getRefreshToken(): string
    {
        return $this->input('refresh_token');
    }
}
