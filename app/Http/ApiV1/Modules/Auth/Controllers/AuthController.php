<?php

namespace App\Http\ApiV1\Modules\Auth\Controllers;

use App\Domain\Auth\Actions\LoginAction;
use App\Domain\Auth\Actions\LogoutAction;
use App\Domain\Auth\Actions\RefreshAction;
use App\Http\ApiV1\Modules\Auth\Requests\LoginRequest;
use App\Http\ApiV1\Modules\Auth\Requests\RefreshRequest;
use App\Http\ApiV1\Modules\Auth\Resources\CurrentUserResource;
use App\Http\ApiV1\Modules\Auth\Resources\TokensResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;

class AuthController
{
    public function login(LoginRequest $request, LoginAction $action): TokensResource
    {
        return TokensResource::make($action->execute($request->getLogin(), $request->getPassword()));
    }

    public function logout(LogoutAction $action): EmptyResource
    {
        $action->execute(user()->accessToken);

        return new EmptyResource();
    }

    public function refresh(RefreshRequest $request, RefreshAction $action): TokensResource
    {
        return TokensResource::make($action->execute($request->getRefreshToken()));
    }

    public function currentUser(): CurrentUserResource
    {
        return CurrentUserResource::make(user());
    }
}
