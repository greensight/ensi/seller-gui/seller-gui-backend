<?php

namespace App\Http\ApiV1\Support\Resources;

abstract class EnumValueResource extends BaseJsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->getEnumId(),
            'title' => $this->getEnumTitle(),
        ];
    }

    abstract protected function getEnumId(): int|string;

    abstract protected function getEnumTitle(): string;
}
