<?php

namespace App\Http\ApiV1\Support\Resources;

use App\Domain\Common\Data\EnumData;

/**
 * @mixin EnumData
 */
class EnumResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
        ];
    }
}
