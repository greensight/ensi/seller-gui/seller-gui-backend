<?php

namespace App\Http\ApiV1\Support\Resources;

use Illuminate\Contracts\Support\Responsable;
use Symfony\Component\HttpFoundation\Response;

class DataResource implements Responsable
{
    private mixed $data;

    public function __construct(mixed $data)
    {
        $this->data = $data;
    }

    public function toResponse($request): Response
    {
        return response()->json(['data' => $this->data]);
    }
}
