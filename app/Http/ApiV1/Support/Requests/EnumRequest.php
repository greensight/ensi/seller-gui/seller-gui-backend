<?php

namespace App\Http\ApiV1\Support\Requests;

use App\Domain\Common\Actions\Queries\Data\EnumRequest as EnumRequestContract;
use App\Domain\Common\Actions\Queries\Data\EnumRequestHttpTrait;

class EnumRequest extends BaseFormRequest implements EnumRequestContract
{
    use EnumRequestHttpTrait;
}
