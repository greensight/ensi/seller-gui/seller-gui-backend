<?php

namespace App\Http\ApiV1\Support\Requests;

use App\Domain\Common\Actions\Queries\Data\SearchRequestHttpTrait;
use App\Domain\Common\Actions\Queries\Data\SearchRequestInterface;

class SearchRequest extends BaseFormRequest implements SearchRequestInterface
{
    use SearchRequestHttpTrait;

    public function isInclude(string $include): bool
    {
        return in_array($include, $this->getInclude());
    }
}
