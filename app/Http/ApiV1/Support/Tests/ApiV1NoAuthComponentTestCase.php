<?php

namespace App\Http\ApiV1\Support\Tests;

use App\Domain\Auth\Models\User;
use App\Http\Middleware\AuthorizationByCustomHeader;

abstract class ApiV1NoAuthComponentTestCase extends ApiV1ComponentTestCase
{
    protected function authorize(User $user = null)
    {
    }

    public function disableMiddlewareForAllTests()
    {
        $this->withMiddleware(AuthorizationByCustomHeader::class);
    }
}
