<?php

namespace App\Http\ApiV1\Support\Tests\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;

abstract class BaseRequestFactory extends BaseApiFactory
{
    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
