<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AuthorizationByCustomHeader
{
    public function handle(Request $request, Closure $next): mixed
    {
        $token = $request->headers->get(config('auth.header_for_token'));
        if ($token) {
            $request->headers->set(
                'Authorization',
                'Bearer ' . $token
            );
        }

        return $next($request);
    }
}
