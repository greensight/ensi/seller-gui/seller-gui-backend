<?php

namespace App\Domain\Common\Data;

class EnumData
{
    public function __construct(public int|string $id, public string $name)
    {
    }

    public static function makeFromEnumDescriptions(array $descriptions): array
    {
        $enums = [];
        foreach ($descriptions as $id => $name) {
            $enums[] = new self($id, $name);
        }

        return $enums;
    }
}
