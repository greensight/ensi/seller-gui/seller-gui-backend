<?php

namespace App\Domain\Common\Data\Meta;

use App\Domain\Common\Data\Meta\Enum\AbstractEnumInfo;
use App\Domain\Common\Data\Meta\Fields\BoolField;
use App\Domain\Common\Data\Meta\Fields\DateField;
use App\Domain\Common\Data\Meta\Fields\DatetimeField;
use App\Domain\Common\Data\Meta\Fields\EmailField;
use App\Domain\Common\Data\Meta\Fields\EnumField;
use App\Domain\Common\Data\Meta\Fields\FloatField;
use App\Domain\Common\Data\Meta\Fields\IdField;
use App\Domain\Common\Data\Meta\Fields\ImageField;
use App\Domain\Common\Data\Meta\Fields\IntField;
use App\Domain\Common\Data\Meta\Fields\PhoneField;
use App\Domain\Common\Data\Meta\Fields\PhotoField;
use App\Domain\Common\Data\Meta\Fields\PluralNumericField;
use App\Domain\Common\Data\Meta\Fields\PriceField;
use App\Domain\Common\Data\Meta\Fields\StringField;

class Field
{
    public static function id(string $code = 'id', string $name = 'ID'): IdField
    {
        return new IdField($code, $name);
    }

    public static function text(string $code, string $name): StringField
    {
        return new StringField($code, $name);
    }

    public static function string(string $code, string $name): StringField
    {
        return self::text($code, $name)->resetFilter();
    }

    public static function keyword(string $code, string $name): StringField
    {
        return self::text($code, $name)->filter();
    }

    public static function boolean(string $code, string $name): BoolField
    {
        return new BoolField($code, $name);
    }

    public static function enum(string $code, string $name, AbstractEnumInfo $enumInfo): EnumField
    {
        return new EnumField($code, $name, $enumInfo);
    }

    public static function datetime(string $code, string $name, bool $inclusiveRange = true): DatetimeField
    {
        return new DatetimeField($code, $name, $inclusiveRange);
    }

    public static function date(string $code, string $name, bool $inclusiveRange = true): DateField
    {
        return new DateField($code, $name, $inclusiveRange);
    }

    public static function integer(string $code, string $name, bool $inclusiveRange = true): IntField
    {
        return new IntField($code, $name, $inclusiveRange);
    }

    public static function pluralNumeric(string $code, string $name): PluralNumericField
    {
        return new PluralNumericField($code, $name);
    }

    public static function float(string $code, string $name, bool $inclusiveRange = true): FloatField
    {
        return new FloatField($code, $name, $inclusiveRange);
    }

    public static function price(string $code, string $name, bool $inclusiveRange = true): PriceField
    {
        return new PriceField($code, $name, $inclusiveRange);
    }

    public static function photo(string $code, string $name): PhotoField
    {
        return new PhotoField($code, $name);
    }

    public static function image(string $code, string $name): ImageField
    {
        return new ImageField($code, $name);
    }

    public static function phone(string $code, string $name): PhoneField
    {
        return new PhoneField($code, $name);
    }

    public static function email(string $code, string $name): EmailField
    {
        return new EmailField($code, $name);
    }
}
