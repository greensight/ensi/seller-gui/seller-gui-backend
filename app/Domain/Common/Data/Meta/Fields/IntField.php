<?php

namespace App\Domain\Common\Data\Meta\Fields;

use App\Http\ApiV1\OpenApiGenerated\Enums\FieldTypeEnum;

class IntField extends AbstractRangeField
{
    protected function type(): FieldTypeEnum
    {
        return FieldTypeEnum::INT;
    }

    protected function init()
    {
        $this->sort();
    }
}
