<?php

namespace App\Domain\Common\Data\Meta\Fields;

use App\Http\ApiV1\OpenApiGenerated\Enums\FieldTypeEnum;

class DatetimeField extends AbstractRangeField
{
    protected function type(): FieldTypeEnum
    {
        return FieldTypeEnum::DATETIME;
    }

    protected function init()
    {
        $this->sort();
    }
}
