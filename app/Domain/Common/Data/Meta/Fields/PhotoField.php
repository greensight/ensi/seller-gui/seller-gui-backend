<?php

namespace App\Domain\Common\Data\Meta\Fields;

use App\Http\ApiV1\OpenApiGenerated\Enums\FieldTypeEnum;

class PhotoField extends AbstractField
{
    protected function type(): FieldTypeEnum
    {
        return FieldTypeEnum::PHOTO;
    }
}
