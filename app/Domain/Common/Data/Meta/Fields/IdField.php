<?php

namespace App\Domain\Common\Data\Meta\Fields;

use App\Http\ApiV1\OpenApiGenerated\Enums\FieldTypeEnum;

class IdField extends AbstractField
{
    public function __construct(string $code = 'id', string $name = 'ID')
    {
        parent::__construct($code, $name);
    }

    protected function type(): FieldTypeEnum
    {
        return FieldTypeEnum::INT;
    }

    protected function init()
    {
        $this->listDefault()->sortDefault()->filter();
    }
}
