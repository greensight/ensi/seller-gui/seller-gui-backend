<?php

namespace App\Domain\Common\Data\Meta\Fields;

abstract class AbstractRangeField extends AbstractField
{
    public function __construct(string $code, string $name, bool $inclusiveRange = true)
    {
        parent::__construct($code, $name);
        $this->setFilterRange($inclusiveRange);

    }

    public function setFilterRange(bool $inclusiveRange): void
    {
        match ($inclusiveRange) {
            true => $this->filterInclusiveRange(),
            false => $this->filterExclusiveRange(),
        };
    }
}
