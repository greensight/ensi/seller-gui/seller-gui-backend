<?php

namespace App\Domain\Common\Data\Meta\Fields;

use App\Http\ApiV1\OpenApiGenerated\Enums\FieldFilterTypeEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\FieldTypeEnum;
use Exception;
use JsonSerializable;

abstract class AbstractField implements JsonSerializable
{
    /** @var bool Поле является ссылкой на детальную страницу */
    public bool $detailLink = false;
    /** @var bool Поле вложено в связанный объект */
    public bool $isObject = false;
    /** @var string|null Код по которому можно загрузить вложенный объект */
    public ?string $include = null;

    /** @var FieldFilterTypeEnum|null Тип фильтра */
    public ?FieldFilterTypeEnum $filter = null;
    /** @var null|string Символьный код поля для фильтрации */
    public ?string $filterKey = null;
    /** @var null|string Символьный код поля для фильтрации по диапазону (ОТ) */
    public ?string $filterRangeKeyFrom = null;
    /** @var null|string Символьный код поля для фильтрации по диапазону (ДО) */
    public ?string $filterRangeKeyTo = null;
    /** @var bool Фильтрация по этому полю выводится по-умолчанию */
    public bool $filterDefault = false;

    /** @var bool Возможна ли сортировка по полю */
    public bool $sort = false;
    /** @var null|string Символьный код поля для сортировки */
    public ?string $sortKey = null;
    /** @var bool Сортировка по этому полю является сортировкой по-умолчанию */
    public bool $sortDefault = false;
    /** @var string Направление сортировки по-умолчанию */
    public string $sortDefaultDirection = 'asc';

    /** @var bool Возможен ли вывод в таблицу */
    public bool $list = true;
    /** @var bool Поле выводится в таблицу по умолчанию */
    public bool $listDefault = false;
    /** @var array Значения типов, для отображения числового поля с несколькими типами */
    public array $valueTypes = [];
    /** @var bool Поле является массивом */
    public bool $isArray = false;

    public function __construct(
        public string $code,
        public string $name,
    ) {
        $this->init();
    }

    /**
     * Получить тип поля
     * @return FieldTypeEnum
     */
    abstract protected function type(): FieldTypeEnum;

    /**
     * Установить настройки поля по умолчанию
     */
    protected function init()
    {
        //
    }

    /**
     * Сбросить настройки фильтрации
     * @return $this
     */
    public function resetFilter(): self
    {
        $this->filter = null;
        $this->filterKey = null;
        $this->filterRangeKeyFrom = null;
        $this->filterRangeKeyTo = null;

        return $this;
    }

    /**
     * Разрешить обычную фильтрацию
     *
     * @param null|string $filterKey Ключ фильтра
     *
     * @return $this
     */
    public function filter(?string $filterKey = null): self
    {
        $this->resetFilter();
        $this->filter = FieldFilterTypeEnum::DEFAULT;
        $this->filterKey = $filterKey ?: $this->code;

        return $this;
    }

    /**
     * Разрешить фильтрацию по подстроке
     *
     * @param null|string $filterKey Ключ фильтра
     *
     * @return $this
     */
    public function filterLike(?string $filterKey = null): self
    {
        $this->resetFilter();
        $this->filter = FieldFilterTypeEnum::LIKE;
        $this->filterKey = $filterKey ?: "{$this->code}_like";

        return $this;
    }

    /**
     * Разрешить фильтрацию с множественным значением
     *
     * @param null|string $filterKey Ключ фильтра
     *
     * @return $this
     */
    public function filterMany(?string $filterKey = null): self
    {
        $this->resetFilter();
        $this->filter = FieldFilterTypeEnum::MANY;
        $this->filterKey = $filterKey ?: $this->code;

        return $this;
    }

    /**
     * Разрешить фильтрацию по диапазону с учетом граничных значений
     */
    public function filterInclusiveRange(?string $filterKeyFrom = null, ?string $filterKeyTo = null): self
    {
        $this->resetFilter();
        $this->filter = FieldFilterTypeEnum::RANGE;
        $this->filterRangeKeyFrom = $filterKeyFrom ?: "{$this->code}_gte";
        $this->filterRangeKeyTo = $filterKeyTo ?: "{$this->code}_lte";

        return $this;
    }

    /**
     * Разрешить фильтрацию по диапазону без учета граничных значений
     */
    public function filterExclusiveRange(?string $filterKeyFrom = null, ?string $filterKeyTo = null): self
    {
        $this->resetFilter();
        $this->filter = FieldFilterTypeEnum::RANGE;
        $this->filterRangeKeyFrom = $filterKeyFrom ?: "{$this->code}_gt";
        $this->filterRangeKeyTo = $filterKeyTo ?: "{$this->code}_lt";

        return $this;
    }

    /**
     * Отметить поле для фильтрации по-умолчанию выводимым
     * @return $this
     */
    public function filterDefault(): self
    {
        if (is_null($this->filter)) {
            throw new Exception("Нельзя установить по-умолчанию не включенный фильтр");
        }
        $this->filterDefault = true;

        return $this;
    }

    /**
     * Включена ли по умолчанию фильтрация по этому полю
     * @return bool
     */
    public function isFilterDefault(): bool
    {
        return $this->filterDefault;
    }

    /**
     * Сбросить настройки сортировки
     * @return $this
     */
    public function resetSort(): self
    {
        $this->sort = false;
        $this->sortKey = null;

        return $this;
    }

    /**
     * Разрешить сортировку по полю
     *
     * @param null|string $sortKey Ключ сортировки
     *
     * @return $this
     */
    public function sort(?string $sortKey = null): self
    {
        if (!$this->list) {
            throw new Exception("Нельзя установить сортировку по полю, которое не выводится в таблицу");
        }
        $this->resetSort();
        $this->sort = true;
        $this->sortKey = $sortKey ?: $this->code;

        return $this;
    }

    /**
     * Разрешить сортировку по полю и отметить поле для сортировки по-умолчанию
     *
     * @param null|string $sortKey Ключ сортировки
     *
     * @return $this
     */
    public function sortDefault(?string $sortKey = null, string $direction = 'asc'): self
    {
        $this->sort($sortKey);
        $this->sortDefault = true;
        $this->sortDefaultDirection = $direction;

        return $this;
    }

    /**
     * Является ли поле сортировкой по-умолчанию
     * @return bool
     */
    public function isSortDefault(): bool
    {
        return $this->sortDefault;
    }

    /**
     * Сбросить фильтр и сортировку
     * @return $this
     */
    public function readOnly(): self
    {
        return $this->resetSort()->resetFilter();
    }

    /**
     * Сбросить настройки отображения в таблице
     * @return $this
     */
    public function resetList(): self
    {
        $this->list = true;
        $this->listDefault = false;

        return $this;
    }

    /**
     * Объект, доступно только отображение
     */
    public function object(?string $include = null): self
    {
        $this->resetSort();
        $this->resetFilter();
        $this->include = $include;
        $this->isObject = true;

        return $this;
    }

    /**
     * Добавление значения типа, для отображения числового поля с несколькими типами (FieldTypeEnum::PLURAL_NUMERIC)
     *
     * @param FieldTypeEnum $valueType - тип фильтра
     * @param int|string $fieldValue - значение поля которому соответствует данный тип
     * @param string $field - имя поля по которому определяется тип
     *
     * @return $this
     */
    public function addValueType(FieldTypeEnum $valueType, int|string $fieldValue, string $field = 'value_type'): self
    {
        $valueTypeData = [
            'type' => $valueType->value,
            'field' => $field,
            'field_value' => $fieldValue,
        ];

        $this->valueTypes[] = $valueTypeData;

        return $this;
    }

    /**
     * Не разрешать вывод в таблицу
     * @return $this
     */
    public function listHide(): self
    {
        $this->resetList();
        $this->resetSort();
        $this->list = false;

        return $this;
    }

    /**
     * Данное поле выводится по умолчанию в таблицу
     * @return $this
     */
    public function listDefault(): self
    {
        $this->resetList();
        $this->listDefault = true;

        return $this;
    }

    /**
     * Является ли поле сортировкой по-умолчанию
     * @return bool
     */
    public function isListDefault(): bool
    {
        return $this->listDefault;
    }

    /**
     * Отметить поле как ссылку на детальную страницу
     * @return $this
     */
    public function detailLink(): self
    {
        $this->detailLink = true;
        $this->listDefault();

        return $this;
    }

    /**
     * Является ли поле ссылкой на детальную страницу
     * @return bool
     */
    public function isDetailLink(): bool
    {
        return $this->detailLink;
    }

    /**
     * Указать поле как массив
     * @return static
     */
    public function isArray(): static
    {
        $this->isArray = true;
        $this->resetSort();
        $this->resetFilter();

        return $this;
    }

    public function jsonSerialize(): array
    {
        $response = [
            'code' => $this->code,
            'is_object' => $this->isObject,
            'include' => $this->include,
            'name' => $this->name,
            'list' => $this->list,
            'is_array' => $this->isArray,
        ];

        $response['type'] = $this->type();

        if ($response['type'] == FieldTypeEnum::PLURAL_NUMERIC) {
            $response['value_types'] = $this->valueTypes;
        }

        $response['sort'] = $this->sort;
        if ($this->sort) {
            $response['sort_key'] = $this->sortKey;
        }

        $response['filter'] = $this->filter;
        if ($this->filter) {
            if ($this->filter == FieldFilterTypeEnum::RANGE) {
                $response['filter_range_key_from'] = $this->filterRangeKeyFrom;
                $response['filter_range_key_to'] = $this->filterRangeKeyTo;
            } else {
                $response['filter_key'] = $this->filterKey;
            }
        }

        return $response;
    }
}
