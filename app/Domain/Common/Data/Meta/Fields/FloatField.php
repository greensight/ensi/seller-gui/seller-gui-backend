<?php

namespace App\Domain\Common\Data\Meta\Fields;

use App\Http\ApiV1\OpenApiGenerated\Enums\FieldTypeEnum;

class FloatField extends AbstractRangeField
{
    protected function type(): FieldTypeEnum
    {
        return FieldTypeEnum::FLOAT;
    }

    protected function init()
    {
        $this->sort();
    }
}
