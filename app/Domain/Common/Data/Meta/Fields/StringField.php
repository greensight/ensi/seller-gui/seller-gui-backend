<?php

namespace App\Domain\Common\Data\Meta\Fields;

use App\Http\ApiV1\OpenApiGenerated\Enums\FieldTypeEnum;

class StringField extends AbstractField
{
    protected function type(): FieldTypeEnum
    {
        return FieldTypeEnum::STRING;
    }

    protected function init()
    {
        $this->filterLike();
    }
}
