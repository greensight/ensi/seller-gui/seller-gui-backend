<?php

namespace App\Domain\Common\Data\Meta\Fields;

use App\Http\ApiV1\OpenApiGenerated\Enums\FieldTypeEnum;

class ImageField extends AbstractField
{
    protected function type(): FieldTypeEnum
    {
        return FieldTypeEnum::IMAGE;
    }

    protected function init()
    {
        $this->readOnly();
    }
}
