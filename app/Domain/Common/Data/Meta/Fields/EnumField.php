<?php

namespace App\Domain\Common\Data\Meta\Fields;

use App\Domain\Common\Data\Meta\Enum\AbstractEnumInfo;
use App\Http\ApiV1\OpenApiGenerated\Enums\FieldTypeEnum;

class EnumField extends AbstractField
{
    public function __construct(string $code, string $name, protected AbstractEnumInfo $enumInfo)
    {
        parent::__construct($code, $name);
    }

    protected function type(): FieldTypeEnum
    {
        return FieldTypeEnum::ENUM;
    }

    protected function init()
    {
        $this->filterMany();
    }

    public function jsonSerialize(): array
    {
        $array = parent::jsonSerialize();
        $array['enum_info'] = $this->enumInfo;

        return $array;
    }
}
