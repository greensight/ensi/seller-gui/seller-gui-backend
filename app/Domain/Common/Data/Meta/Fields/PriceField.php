<?php

namespace App\Domain\Common\Data\Meta\Fields;

use App\Http\ApiV1\OpenApiGenerated\Enums\FieldTypeEnum;

class PriceField extends AbstractRangeField
{
    protected function type(): FieldTypeEnum
    {
        return FieldTypeEnum::PRICE;
    }

    protected function init()
    {
        $this->sort();
    }
}
