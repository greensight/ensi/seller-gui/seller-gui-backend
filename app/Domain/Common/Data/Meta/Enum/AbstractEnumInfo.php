<?php

namespace App\Domain\Common\Data\Meta\Enum;

use Exception;
use JsonSerializable;

abstract class AbstractEnumInfo implements JsonSerializable
{
    protected ?string $endpointName = null;
    protected array $endpointParams = [];
    protected array $values = [];

    protected function addValue(string $id, string $title): self
    {
        $this->values[] = ['id' => $id, 'title' => $title];

        return $this;
    }

    protected function addValues(array $sources): void
    {
        foreach ($sources as $source) {
            $this->addValue($source->getId(), $source->getName());
        }
    }

    public function jsonSerialize(): array
    {
        $result = [];
        if ($this->endpointName) {
            $result['endpoint'] = route($this->endpointName, $this->endpointParams, false);
        } elseif ($this->values) {
            $result['values'] = $this->values;
        } else {
            throw new Exception("Необходимо заполнить инфо о перечислении");
        }

        return $result;
    }

    /** @throws Exception */
    protected function enumClassToValues(string $enumClass): void
    {
        if (!method_exists($enumClass, 'getDescriptions')) {
            throw new Exception('Method getDescriptions not exists');
        }

        foreach ($enumClass::getDescriptions() as $id => $title) {
            $this->addValue($id, $title);
        }
    }
}
