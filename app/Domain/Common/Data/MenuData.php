<?php

namespace App\Domain\Common\Data;

use App\Http\ApiV1\OpenApiGenerated\Enums\MenuItemCodeEnum;

class MenuData
{
    public static function menu(): array
    {
        return [
            // Настройки
            MenuItemCodeEnum::SETTINGS->value => [
                'items' => [
                    // Основные данные
                    MenuItemCodeEnum::SETTINGS_MAIN_DATA->value => [],
                    // Склады
                    MenuItemCodeEnum::SETTINGS_STORES->value => [],
                    // Пользователи
                    MenuItemCodeEnum::SETTINGS_USERS->value => [],
                ],
            ],
        ];
    }
}
