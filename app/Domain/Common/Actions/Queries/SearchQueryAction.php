<?php

namespace App\Domain\Common\Actions\Queries;

use App\Domain\Common\Actions\Queries\Data\SearchRequestInterface;
use App\Domain\Common\Actions\Queries\Steps\SetFilterAction;
use App\Domain\Common\Actions\Queries\Steps\SetIncludeAction;
use App\Domain\Common\Actions\Queries\Steps\SetPaginationAction;
use App\Domain\Common\Actions\Queries\Steps\SetSortAction;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use App\Http\ApiV1\Support\Pagination\Page;

class SearchQueryAction
{
    public function __construct(
        protected readonly SetFilterAction $setFilterAction,
        protected readonly SetSortAction $setSortAction,
        protected readonly SetPaginationAction $setPaginationAction,
        protected readonly SetIncludeAction $setIncludeAction,
    ) {
    }

    public function execute(
        SearchRequestInterface $searchRequest,
        string $requestClass,
        callable $search,
        ?callable $convertItems = null,
        array $requiredInclude = [],
        array $excludeInclude = [],
        array $requiredFilter = [],
    ): Page {
        $apiRequest = new $requestClass();

        $this->setFilterAction->execute($apiRequest, $searchRequest, $requiredFilter);
        $this->setSortAction->execute($apiRequest, $searchRequest);
        $this->setPaginationAction->execute($apiRequest, $searchRequest);
        $this->setIncludeAction->execute($apiRequest, $searchRequest, $requiredInclude, $excludeInclude);

        $response = $search($apiRequest);

        $items = $response->getData();
        if ($convertItems) {
            $items = $convertItems($items);
        }

        $pagination = $response->getMeta()->getPagination();

        return new Page($items, $pagination->getType() == PaginationTypeEnum::OFFSET->value ? [
            "limit" => $pagination->getLimit(),
            "offset" => $pagination->getOffset(),
            "total" => $pagination->getTotal(),
            "type" => $pagination->getType(),
        ] : [
            "cursor" => $pagination->getCursor(),
            "limit" => $pagination->getLimit(),
            "next_cursor" => $pagination->getNextCursor(),
            "previous_cursor" => $pagination->getPreviousCursor(),
            "type" => $pagination->getType(),
        ]);
    }
}
