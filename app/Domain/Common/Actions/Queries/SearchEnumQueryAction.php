<?php

namespace App\Domain\Common\Actions\Queries;

use App\Domain\Common\Actions\Queries\Data\EnumRequest;
use App\Domain\Common\Actions\Queries\Data\PaginateRequestInterface;
use App\Domain\Common\Actions\Queries\Steps\SetFilterAction;
use App\Domain\Common\Actions\Queries\Steps\SetIncludeAction;
use App\Domain\Common\Actions\Queries\Steps\SetPaginationAction;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use Illuminate\Support\Collection;

class SearchEnumQueryAction
{
    public function __construct(
        protected readonly SetFilterAction $setFilterAction,
        protected readonly SetIncludeAction $setIncludeAction,
        protected readonly SetPaginationAction $setPaginationAction,
    ) {
    }

    public function execute(
        EnumRequest $enumRequest,
        string $requestClass,
        callable $convertFilter,
        callable $search,
        ?callable $convertItems = null,
        array $requiredInclude = [],
        array $requiredFilter = [],
    ): Collection {
        $apiRequest = new $requestClass();

        $this->setFilterAction->execute(
            $apiRequest,
            requiredFilter: array_merge(
                array_filter($convertFilter($enumRequest)),
                $requiredFilter
            )
        );
        $this->setIncludeAction->execute($apiRequest, requiredInclude: $requiredInclude);
        $this->setPagination($apiRequest, $enumRequest);

        $response = $search($apiRequest);

        $items = $response->getData();
        if ($convertItems) {
            $items = $convertItems($items);
        }

        return collect($items);
    }

    protected function setPagination($apiRequest, EnumRequest $enumRequest): void
    {
        $this->setPaginationAction->execute($apiRequest, new class ($enumRequest) implements PaginateRequestInterface {
            public function __construct(protected EnumRequest $enumRequest)
            {
            }

            public function getPagination(): array
            {
                $arr = ['type' => PaginationTypeEnum::CURSOR];

                $ids = $this->enumRequest->getIds();
                if ($ids) {
                    $arr['limit'] = count($ids);
                }

                return $arr;
            }
        });
    }
}
