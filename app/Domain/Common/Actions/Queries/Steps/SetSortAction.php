<?php

namespace App\Domain\Common\Actions\Queries\Steps;

use App\Domain\Common\Actions\Queries\Data\SearchRequestInterface;

class SetSortAction
{
    public function execute($apiRequest, SearchRequestInterface $searchRequest): void
    {
        if ($searchRequest->getSort()) {
            $apiRequest->setSort($searchRequest->getSort());
        }
    }
}
