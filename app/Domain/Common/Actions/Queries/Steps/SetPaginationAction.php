<?php

namespace App\Domain\Common\Actions\Queries\Steps;

use App\Domain\Common\Actions\Queries\Data\PaginateRequestInterface;

class SetPaginationAction
{
    public function execute($apiRequest, PaginateRequestInterface $searchRequest): void
    {
        if ($searchRequest->getPagination()) {
            $class = $apiRequest::openAPITypes()['pagination'];
            $apiRequest->setPagination(new $class($searchRequest->getPagination()));
        }
    }
}
