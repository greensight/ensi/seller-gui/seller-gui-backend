<?php

namespace App\Domain\Common\Actions\Queries\Steps;

use App\Domain\Common\Actions\Queries\Data\SearchRequestInterface;

class MakeIncludeAction
{
    public function execute(?SearchRequestInterface $searchRequest = null, array $requiredInclude = [], array $excludeInclude = []): array
    {
        // Важно - сначала getHttpInclude, а потом include
        return array_unique(array_diff(array_merge($searchRequest?->getInclude() ?: [], $requiredInclude), $excludeInclude));
    }
}
