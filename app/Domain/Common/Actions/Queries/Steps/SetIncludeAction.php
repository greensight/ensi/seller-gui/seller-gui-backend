<?php

namespace App\Domain\Common\Actions\Queries\Steps;

use App\Domain\Common\Actions\Queries\Data\SearchRequestInterface;

class SetIncludeAction
{
    public function __construct(
        protected MakeIncludeAction $makeIncludeAction,
    ) {
    }

    public function execute($apiRequest, ?SearchRequestInterface $searchRequest = null, array $requiredInclude = [], array $excludeInclude = []): void
    {
        $include = $this->makeIncludeAction->execute($searchRequest, $requiredInclude, $excludeInclude);

        if ($include) {
            $apiRequest->setInclude($include);
        }
    }
}
