<?php

namespace App\Domain\Common\Actions\Queries\Steps;

use App\Domain\Common\Actions\Queries\Data\SearchRequestInterface;

class SetFilterAction
{
    public function execute($apiRequest, ?SearchRequestInterface $searchRequest = null, array $requiredFilter = []): void
    {
        $filter = array_merge($searchRequest?->getFilter() ?: [], $requiredFilter);
        if ($filter) {
            $apiRequest->setFilter((object)$filter);
        }
    }
}
