<?php

namespace App\Domain\Common\Actions\Queries;

use App\Domain\Common\Actions\Queries\Data\SearchRequestInterface;
use App\Domain\Common\Actions\Queries\Steps\SetFilterAction;
use App\Domain\Common\Actions\Queries\Steps\SetIncludeAction;

class SearchOneQueryAction
{
    public function __construct(
        protected readonly SetFilterAction $setFilterAction,
        protected readonly SetIncludeAction $setIncludeAction,
    ) {
    }

    public function execute(
        SearchRequestInterface $searchRequest,
        string $requestClass,
        callable $searchOne,
        ?callable $convertItem = null,
        array $requiredInclude = [],
        array $excludeInclude = [],
        array $requiredFilter = [],
    ): mixed {
        $apiRequest = new $requestClass();

        $this->setFilterAction->execute($apiRequest, $searchRequest, $requiredFilter);
        $this->setIncludeAction->execute($apiRequest, $searchRequest, $requiredInclude, $excludeInclude);

        $response = $searchOne($apiRequest);

        $item = $response->getData();
        if ($convertItem) {
            $item = $convertItem($item);
        }

        return $item;
    }
}
