<?php

namespace App\Domain\Common\Actions\Queries;

use App\Domain\Common\Actions\Queries\Data\SearchRequestInterface;
use App\Domain\Common\Actions\Queries\Steps\MakeIncludeAction;

class SearchByIdQueryAction
{
    public function __construct(
        protected readonly MakeIncludeAction $makeIncludeAction,
    ) {
    }

    public function execute(
        int|string $id,
        callable $searchById,
        ?SearchRequestInterface $searchRequest = null,
        ?callable $convertItem = null,
        array $requiredInclude = [],
    ): mixed {
        $response = $searchById($id, $this->makeIncludeAction->execute($searchRequest, $requiredInclude));
        $item = $response->getData();
        if ($convertItem) {
            $item = $convertItem($item);
        }

        return $item;
    }
}
