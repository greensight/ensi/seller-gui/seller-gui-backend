<?php

namespace App\Domain\Common\Actions\Queries\Data;

interface PaginateRequestInterface
{
    public function getPagination(): ?array;
}
