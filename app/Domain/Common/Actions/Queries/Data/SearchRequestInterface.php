<?php

namespace App\Domain\Common\Actions\Queries\Data;

interface SearchRequestInterface extends PaginateRequestInterface
{
    public function getFilter(): array;

    public function getInclude(): array;

    public function getSort(): ?array;
}
