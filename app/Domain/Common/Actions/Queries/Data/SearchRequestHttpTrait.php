<?php

namespace App\Domain\Common\Actions\Queries\Data;

use Illuminate\Http\Request;

/** @mixin Request */
trait SearchRequestHttpTrait
{
    public function getFilter(): array
    {
        return $this->get('filter', []);
    }

    public function getInclude(): array
    {
        $include = $this->get('include', []);

        if (is_string($include)) {
            $include = explode(',', $include);
        }

        return $include;
    }

    public function getSort(): ?array
    {
        return $this->get('sort');
    }

    public function getPagination(): ?array
    {
        return $this->get('pagination');
    }
}
