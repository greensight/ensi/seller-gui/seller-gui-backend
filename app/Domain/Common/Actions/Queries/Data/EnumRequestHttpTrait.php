<?php

namespace App\Domain\Common\Actions\Queries\Data;

use Illuminate\Http\Request;

/** @mixin Request */
trait EnumRequestHttpTrait
{
    public function getIds(): ?array
    {
        return data_get($this->get('filter', []), 'id');
    }

    public function getQuery(): ?string
    {
        return data_get($this->get('filter', []), 'query');
    }
}
