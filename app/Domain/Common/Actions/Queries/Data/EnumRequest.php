<?php

namespace App\Domain\Common\Actions\Queries\Data;

interface EnumRequest
{
    public function getIds(): ?array;

    public function getQuery(): ?string;
}
