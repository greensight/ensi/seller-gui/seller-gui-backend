<?php

namespace App\Domain\Common\Actions;

use App\Domain\Common\Data\MenuData;

class GetMenuItemsForUserAction
{
    public function execute(): array
    {
        $menu = MenuData::menu();
        $menuItems = $this->getMenuWithAccessRight($menu);

        return $this->getMenuItemCodes($menuItems);
    }

    /**
     * Получить пункты меню текущего уровня вложенности, к которому у пользователя есть права доступа
     * @param array $menu - пункты меню для текущего уровня вложенности и все вложенные в них пункты меню
     */
    protected function getMenuWithAccessRight(array &$menu): array
    {
        foreach ($menu as $code => &$menuItem) {
            if (!empty($menuItem['items'])) {
                $menuItem['items'] = $this->getMenuWithAccessRight($menuItem['items']);
            }
        }

        return $menu;
    }

    /**
     * Получить коды пунктов меню со всех уровней вложенности
     * @param  array  $menuItems
     * @return array
     */
    protected function getMenuItemCodes(array $menuItems): array
    {
        $codes = [];

        foreach ($menuItems as $code => $menuItem) {
            $skip = array_key_exists('items', $menuItem) && count($menuItem['items']) == 0;
            if (!$skip) {
                $codes[] = $code;
                if (!empty($menuItem['items'])) {
                    $codes = array_merge($codes, $this->getMenuItemCodes($menuItem['items']));
                }
            }
        }

        return $codes;
    }
}
