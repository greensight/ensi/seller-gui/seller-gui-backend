<?php

namespace App\Domain\Auth\Actions;

use Ensi\SellerAuthClient\Api\OauthApi;
use Ensi\SellerAuthClient\Dto\CreateTokenRequest;
use Ensi\SellerAuthClient\Dto\CreateTokenResponse;
use Ensi\SellerAuthClient\Dto\GrantTypeEnum;

class LoginAction
{
    public function __construct(protected readonly OauthApi $oauthApi)
    {
    }

    public function execute(string $login, string $password): CreateTokenResponse
    {
        $request = new CreateTokenRequest();
        $request->setGrantType(GrantTypeEnum::PASSWORD);
        $request->setClientId(config('openapi-clients.units.seller-auth.client.id'));
        $request->setClientSecret(config('openapi-clients.units.seller-auth.client.secret'));
        $request->setUsername($login);
        $request->setPassword($password);

        return $this->oauthApi->createToken($request);
    }
}
