<?php

namespace App\Domain\Auth\Actions;

use Ensi\SellerAuthClient\Api\OauthApi;
use Lcobucci\JWT\Encoding\JoseEncoder;
use Lcobucci\JWT\Token\Parser;
use Lcobucci\JWT\Token\Plain;

class LogoutAction
{
    public function __construct(protected readonly OauthApi $oauthApi)
    {
    }

    public function execute(string $token): void
    {
        /** @var Plain $plain */
        $plain = (new Parser(new JoseEncoder()))->parse($token);
        $tokenId = $plain->claims()->all()['jti'];

        $this->oauthApi->getConfig()->setAccessToken($token);
        $this->oauthApi->deleteToken($tokenId);
    }
}
