<?php

namespace App\Domain\Auth\Actions;

use App\Domain\Auth\Models\User;
use Ensi\SellerAuthClient\Api\UsersApi;
use Ensi\SellerAuthClient\ApiException;

class LoadUserByTokenAction
{
    public function __construct(protected readonly UsersApi $usersApi)
    {
    }

    public function execute(string $token): ?User
    {
        try {
            $this->usersApi->getConfig()->setAccessToken($token);
            $response = $this->usersApi->getCurrentUser();

            $user = $response->getData();

            return new User(
                id: $user->getId(),
                sellerId: $user->getSellerId(),
                login: $user->getLogin(),
                active: $user->getActive(),
                fullName: $user->getFullName(),
                shortName: $user->getShortName(),
                firstName: $user->getFirstName(),
                lastName: $user->getLastName(),
                middleName: $user->getMiddleName(),
                phone: $user->getPhone(),
                email: $user->getEmail(),
                accessToken: $token,
            );
        } catch (ApiException $e) {
            return null;
        }
    }
}
