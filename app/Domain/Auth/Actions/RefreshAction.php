<?php

namespace App\Domain\Auth\Actions;

use Ensi\SellerAuthClient\Api\OauthApi;
use Ensi\SellerAuthClient\Dto\CreateTokenRequest;
use Ensi\SellerAuthClient\Dto\CreateTokenResponse;
use Ensi\SellerAuthClient\Dto\GrantTypeEnum;

class RefreshAction
{
    public function __construct(protected readonly OauthApi $oauthApi)
    {
    }

    public function execute(string $refreshToken): CreateTokenResponse
    {
        $request = new CreateTokenRequest();
        $request->setGrantType(GrantTypeEnum::REFRESH_TOKEN);
        $request->setClientId(config('openapi-clients.units.seller-auth.client.id'));
        $request->setClientSecret(config('openapi-clients.units.seller-auth.client.secret'));
        $request->setRefreshToken($refreshToken);

        return $this->oauthApi->createToken($request);
    }
}
