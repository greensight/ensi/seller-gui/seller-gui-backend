<?php

use App\Domain\Auth\Traits\HandlesRules;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Illuminate\Auth\Access\Response as AccessResponse;

uses(ApiV1ComponentTestCase::class)->group('categories', 'component');

test('Trait HandlesRules allowed', function (
    array $data,
    array $userPermissions,
    int $mainPermission,
    array $permissionsFields,
) {
    $rulesAction = new class () {
        use HandlesRules;
    };

    /** @var AccessResponse $response */
    $response = $rulesAction->allowFieldsOf($data, $userPermissions, $mainPermission, $permissionsFields);

    expect($response->allowed())->toBe(true);
})->with([
    'main permissions' => [['foo' => '1'], [1000], 1000, []],
    'field permissions' => [['foo' => '1'], [1000], 2000, [1000 => ['foo']]],
    'some field permissions' => [['foo' => '1',  'baz' => 2], [1000], 2000, [1000 => ['foo', 'baz']]],
    'cross field permissions' => [['foo' => '1', 'baz' => 2], [1000, 1001], 2000, [1000 => ['foo'], 1001 => ['baz']]],
]);

test('Trait HandlesRules denied', function (
    array $data,
    array $userPermissions,
    int $mainPermission,
    array $permissionsFields,
) {
    $rulesAction = new class () {
        use HandlesRules;
    };

    /** @var AccessResponse $response */
    $response = $rulesAction->allowFieldsOf($data, $userPermissions, $mainPermission, $permissionsFields);

    expect($response->denied())->toBe(true);
})->with([
    'main permissions' => [['foo' => '1'], [1000], 2000, []],
    'field permissions' => [['foo' => '1'], [1000], 2000, [3000 => ['foo']]],
    'only one matches in permissions' => [['foo' => '1',  'baz' => 2], [1000], 2000, [1000 => ['foo']]],
    'only one matches in cross permissions' => [['foo' => '1', 'baz' => 2], [1000, 1001], 2000, [1000 => ['foo']]],
]);
