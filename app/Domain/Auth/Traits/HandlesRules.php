<?php

namespace App\Domain\Auth\Traits;

use Illuminate\Auth\Access\Response as AccessResponse;

trait HandlesRules
{
    /**
     * @param array $data - данные передаваемые для создания/редактирования
     * @param array $userPermissions - правила пользователя
     * @param int $mainPermission - главное правило, которое позволяет взаимодействовать со всеми полями
     * @param array<int, array<int, string>> $permissionsFields - массив описывающий какие правила позволяют взаимодействовать с какими полями
     */
    public function allowFieldsOf(array $data, array $userPermissions, int $mainPermission, array $permissionsFields): AccessResponse
    {
        if ($this->checkPermission($mainPermission, $userPermissions)) {
            return AccessResponse::allow();
        }

        $fieldsPermissions = $this->flip($permissionsFields);

        foreach ($data as $key => $value) {
            $permissionField = data_get($fieldsPermissions, $key);
            if ($permissionField === null) {
                return AccessResponse::denyWithStatus(403);
            }

            if ($this->checkPermission($permissionField, $userPermissions)) {
                unset($data[$key]);
            }
        }

        return count($data) === 0 ? AccessResponse::allow() : AccessResponse::denyWithStatus(403);
    }

    protected function flip(array $permissionsFields): array
    {
        $fieldsPermissions = [];
        foreach ($permissionsFields as $permission => $fields) {
            foreach ($fields as $field) {
                $fieldsPermissions[$field] = $permission;
            }
        }

        return $fieldsPermissions;
    }

    protected function checkPermission(int $permission, array $userPermissions): bool
    {
        return in_array($permission, $userPermissions);
    }
}
