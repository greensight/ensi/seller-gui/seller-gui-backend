<?php

namespace App\Domain\Auth\Models\Tests\Factories;

use App\Domain\Common\Tests\Factories\JWTFactory;
use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\SellerAuthClient\Dto\CreateTokenResponse;

class TokenFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'access_token' => JWTFactory::new()->make(),
            'refresh_token' => JWTFactory::new()->make(),
            'expires_in' => $this->faker->randomNumber(),
        ];
    }

    public function make(array $extra = []): CreateTokenResponse
    {
        return new CreateTokenResponse($this->makeArray($extra));
    }
}
