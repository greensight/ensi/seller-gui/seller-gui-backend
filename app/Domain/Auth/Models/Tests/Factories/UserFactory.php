<?php

namespace App\Domain\Auth\Models\Tests\Factories;

use App\Domain\Auth\Models\User;
use App\Domain\Common\Tests\Factories\JWTFactory;
use Ensi\LaravelTestFactories\BaseApiFactory;

class UserFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'seller_id' => $this->faker->modelId(),
            'login' => $this->faker->unique()->userName(),
            'active' => $this->faker->boolean(),
            'full_name' => $this->faker->name(),
            'short_name' => $this->faker->name(),
            'last_name' => $this->faker->lastName(),
            'first_name' => $this->faker->firstName(),
            'middle_name' => $this->faker->firstName(),
            'phone' => $this->faker->unique()->numerify('+7##########'),
            'email' => $this->faker->unique()->safeEmail(),
            'token' => JWTFactory::new()->make(),
        ];
    }

    public function withSeller(int $sellerId): self
    {
        return $this->state(['seller_id' => $sellerId]);
    }

    public function make(array $extra = []): User
    {
        $userData = $this->makeArray($extra);

        return new User(
            id: $userData['id'],
            sellerId: $userData['seller_id'],
            login: $userData['login'],
            active: $userData['active'],
            fullName: $userData['full_name'],
            shortName: $userData['short_name'],
            firstName: $userData['first_name'],
            lastName: $userData['last_name'],
            middleName: $userData['middle_name'],
            phone: $userData['phone'],
            email: $userData['email'],
            accessToken: $userData['token'],
        );
    }
}
