<?php

namespace App\Domain\Auth\Models;

use App\Domain\Auth\Models\Tests\Factories\UserFactory;
use Illuminate\Contracts\Auth\Authenticatable;

class User implements Authenticatable
{
    public function __construct(
        public readonly int $id,
        public readonly int $sellerId,
        public readonly string $login,
        public readonly bool $active,
        public readonly string $fullName,
        public readonly string $shortName,
        public readonly string $firstName,
        public readonly string $lastName,
        public readonly string $middleName,
        public readonly string $phone,
        public readonly string $email,
        public readonly string $accessToken,
    ) {
    }

    /**
     * Возвращает логин пользователя.
     */
    public function getAuthIdentifierName(): string
    {
        return $this->login;
    }

    /**
     * Возвращает идентификатор пользователя.
     */
    public function getAuthIdentifier(): int
    {
        return $this->id;
    }

    public function getAuthPassword(): string
    {
        return '';
    }

    public function getRememberToken(): string
    {
        return '';
    }

    public function setRememberToken($value): void
    {
    }

    public function getRememberTokenName(): string
    {
        return 'remember_token';
    }

    public function getAuthPasswordName(): string
    {
        return '';
    }

    public static function factory(): UserFactory
    {
        return UserFactory::new();
    }
}
