<?php

namespace App\Domain\Units\Actions\Sellers;

use Ensi\BuClient\Api\SellersApi;
use Ensi\BuClient\ApiException;
use Ensi\BuClient\Dto\PatchSellerRequest;
use Ensi\BuClient\Dto\Seller;

class PatchSellerAction
{
    public function __construct(protected readonly SellersApi $sellersApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(array $fields): Seller
    {
        $seller = new PatchSellerRequest($fields);

        return $this->sellersApi->patchSeller(user()->sellerId, $seller)->getData();
    }
}
