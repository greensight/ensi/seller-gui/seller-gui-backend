<?php

namespace App\Domain\Units\Actions\Sellers;

use App\Domain\Common\Actions\Queries\Data\SearchRequestInterface;
use App\Domain\Common\Actions\Queries\SearchByIdQueryAction;
use Ensi\BuClient\Api\SellersApi;
use Ensi\BuClient\ApiException;
use Ensi\BuClient\Dto\Seller;

class GetSellerAction
{
    public function __construct(
        protected readonly SearchByIdQueryAction $searchByIdQueryAction,
        protected readonly SellersApi $sellersApi
    ) {
    }

    /**
     * @throws ApiException
     */
    public function execute(?SearchRequestInterface $searchRequest = null): Seller
    {
        return $this->searchByIdQueryAction->execute(
            id: user()->sellerId,
            searchById: fn (int $id, array $include) => $this->sellersApi->getSeller($id, implode(",", $include)),
            searchRequest: $searchRequest,
        );
    }
}
