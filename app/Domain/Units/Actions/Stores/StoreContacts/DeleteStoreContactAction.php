<?php

namespace App\Domain\Units\Actions\Stores\StoreContacts;

use Ensi\BuClient\Api\StoreContactsApi;
use Ensi\BuClient\Dto\FilterStoreContactsRequest;

class DeleteStoreContactAction
{
    public function __construct(protected readonly StoreContactsApi $storeContactsApi)
    {
    }

    public function execute(int $id): void
    {
        $filterRequest = new FilterStoreContactsRequest();
        $filterRequest->setFilter((object)[
            'seller_id' => user()->sellerId,
        ]);

        $this->storeContactsApi->deleteStoreContact($id, $filterRequest);
    }
}
