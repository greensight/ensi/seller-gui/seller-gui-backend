<?php

namespace App\Domain\Units\Actions\Stores\StoreContacts;

use Ensi\BuClient\Api\StoreContactsApi;
use Ensi\BuClient\ApiException;
use Ensi\BuClient\Dto\CreateStoreContactRequest;
use Ensi\BuClient\Dto\StoreContact;

class CreateStoreContactAction
{
    public function __construct(protected readonly StoreContactsApi $storeContactsApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(array $fields): StoreContact
    {
        $request = new CreateStoreContactRequest($fields);
        $request->setSellerId(user()->sellerId);

        return $this->storeContactsApi->createStoreContact($request)->getData();
    }
}
