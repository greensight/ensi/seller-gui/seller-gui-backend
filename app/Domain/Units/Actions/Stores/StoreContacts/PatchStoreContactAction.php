<?php

namespace App\Domain\Units\Actions\Stores\StoreContacts;

use Ensi\BuClient\Api\StoreContactsApi;
use Ensi\BuClient\ApiException;
use Ensi\BuClient\Dto\ExtendedPatchStoreContactRequest;
use Ensi\BuClient\Dto\PatchStoreContactRequest;
use Ensi\BuClient\Dto\StoreContact;

class PatchStoreContactAction
{
    public function __construct(protected readonly StoreContactsApi $storeContactsApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $id, array $fields): StoreContact
    {
        $request = new ExtendedPatchStoreContactRequest();
        $request->setFields(new PatchStoreContactRequest($fields));
        $request->setFilter((object)[
            'seller_id' => user()->sellerId,
        ]);

        return $this->storeContactsApi->extendedPatchStoreContact($id, $request)->getData();
    }
}
