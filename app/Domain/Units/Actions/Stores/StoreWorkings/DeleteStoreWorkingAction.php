<?php

namespace App\Domain\Units\Actions\Stores\StoreWorkings;

use Ensi\BuClient\Api\StoreWorkingsApi;
use Ensi\BuClient\Dto\FilterStoreWorkingsRequest;

class DeleteStoreWorkingAction
{
    public function __construct(protected readonly StoreWorkingsApi $storeWorkingsApi)
    {
    }

    public function execute(int $id): void
    {
        $filterRequest = new FilterStoreWorkingsRequest();
        $filterRequest->setFilter((object)[
            'seller_id' => user()->sellerId,
        ]);

        $this->storeWorkingsApi->deleteStoreWorking($id, $filterRequest);
    }
}
