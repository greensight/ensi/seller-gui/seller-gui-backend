<?php

namespace App\Domain\Units\Actions\Stores\StoreWorkings;

use Ensi\BuClient\Api\StoreWorkingsApi;
use Ensi\BuClient\ApiException;
use Ensi\BuClient\Dto\CreateStoreWorkingRequest;
use Ensi\BuClient\Dto\StoreWorking;

class CreateStoreWorkingAction
{
    public function __construct(protected readonly StoreWorkingsApi $storeWorkingsApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(array $fields): StoreWorking
    {
        $request = new CreateStoreWorkingRequest($fields);
        $request->setSellerId(user()->sellerId);

        return $this->storeWorkingsApi->createStoreWorking($request)->getData();
    }
}
