<?php

namespace App\Domain\Units\Actions\Stores\StoreWorkings;

use Ensi\BuClient\Api\StoreWorkingsApi;
use Ensi\BuClient\ApiException;
use Ensi\BuClient\Dto\ExtendedPatchStoreWorkingRequest;
use Ensi\BuClient\Dto\PatchStoreWorkingRequest;
use Ensi\BuClient\Dto\StoreWorking;

class PatchStoreWorkingAction
{
    public function __construct(protected readonly StoreWorkingsApi $storeWorkingsApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $id, array $fields): StoreWorking
    {
        $request = new ExtendedPatchStoreWorkingRequest();
        $request->setFields(new PatchStoreWorkingRequest($fields));
        $request->setFilter((object)[
            'seller_id' => user()->sellerId,
        ]);

        return $this->storeWorkingsApi->extendedPatchStoreWorking($id, $request)->getData();
    }
}
