<?php

namespace App\Domain\Units\Actions\Stores\StorePickupTimes;

use Ensi\BuClient\Api\StorePickupTimesApi;
use Ensi\BuClient\ApiException;
use Ensi\BuClient\Dto\ExtendedPatchStorePickupTimeRequest;
use Ensi\BuClient\Dto\PatchStorePickupTimeRequest;
use Ensi\BuClient\Dto\StorePickupTime;

class PatchStorePickupTimeAction
{
    public function __construct(protected readonly StorePickupTimesApi $storePickupTimesApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $id, array $fields): StorePickupTime
    {
        $request = new ExtendedPatchStorePickupTimeRequest();
        $request->setFields(new PatchStorePickupTimeRequest($fields));
        $request->setFilter((object)[
            'seller_id' => user()->sellerId,
        ]);

        return $this->storePickupTimesApi->extendedPatchStorePickupTime($id, $request)->getData();
    }
}
