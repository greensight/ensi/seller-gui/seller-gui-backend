<?php

namespace App\Domain\Units\Actions\Stores\StorePickupTimes;

use Ensi\BuClient\Api\StorePickupTimesApi;
use Ensi\BuClient\Dto\FilterStorePickupTimesRequest;

class DeleteStorePickupTimeAction
{
    public function __construct(protected readonly StorePickupTimesApi $storePickupTimesApi)
    {
    }

    public function execute(int $id): void
    {
        $filterRequest = new FilterStorePickupTimesRequest();
        $filterRequest->setFilter((object)[
            'seller_id' => user()->sellerId,
        ]);

        $this->storePickupTimesApi->deleteStorePickupTime($id, $filterRequest);
    }
}
