<?php

namespace App\Domain\Units\Actions\Stores\Stores;

use App\Domain\Common\Actions\Queries\Data\SearchRequestInterface;
use App\Domain\Common\Actions\Queries\SearchOneQueryAction;
use Ensi\BuClient\Api\StoresApi;
use Ensi\BuClient\ApiException;
use Ensi\BuClient\Dto\SearchStoresRequest;
use Ensi\BuClient\Dto\Store;

class GetStoreAction
{
    public function __construct(
        protected readonly SearchOneQueryAction $searchOneQueryAction,
        protected readonly StoresApi $api,
    ) {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $id, SearchRequestInterface $searchRequest): Store
    {
        return $this->searchOneQueryAction->execute(
            searchRequest: $searchRequest,
            requestClass: SearchStoresRequest::class,
            searchOne: fn (SearchStoresRequest $request) => $this->api->searchOneStore($request),
            requiredFilter: [
                'id' => $id,
                'seller_id' => user()->sellerId,
            ]
        );
    }
}
