<?php

namespace App\Domain\Units\Actions\Stores\Stores;

use Ensi\BuClient\Api\StoresApi;
use Ensi\BuClient\ApiException;
use Ensi\BuClient\Dto\CreateStoreRequest;
use Ensi\BuClient\Dto\Store;

class CreateStoreAction
{
    public function __construct(protected readonly StoresApi $storesApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(array $fields): Store
    {
        $request = new CreateStoreRequest($fields);
        $request->setSellerId(user()->sellerId);

        return $this->storesApi->createStore($request)->getData();
    }
}
