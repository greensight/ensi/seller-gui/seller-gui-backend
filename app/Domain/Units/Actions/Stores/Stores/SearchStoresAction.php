<?php

namespace App\Domain\Units\Actions\Stores\Stores;

use App\Domain\Common\Actions\Queries\Data\SearchRequestInterface;
use App\Domain\Common\Actions\Queries\SearchQueryAction;
use App\Http\ApiV1\Support\Pagination\Page;
use Ensi\BuClient\Api\StoresApi;
use Ensi\BuClient\Dto\SearchStoresRequest;

class SearchStoresAction
{
    public function __construct(
        protected readonly SearchQueryAction $searchQueryAction,
        protected readonly StoresApi $api,
    ) {
    }

    public function execute(SearchRequestInterface $searchRequest): Page
    {
        return $this->searchQueryAction->execute(
            searchRequest: $searchRequest,
            requestClass: SearchStoresRequest::class,
            search: fn (SearchStoresRequest $request) => $this->api->searchStores($request),
            requiredFilter: [
                'seller_id' => user()->sellerId,
            ]
        );
    }
}
