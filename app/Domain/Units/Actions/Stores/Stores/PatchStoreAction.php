<?php

namespace App\Domain\Units\Actions\Stores\Stores;

use Ensi\BuClient\Api\StoresApi;
use Ensi\BuClient\ApiException;
use Ensi\BuClient\Dto\ExtendedPatchStoreRequest;
use Ensi\BuClient\Dto\PatchStoreRequest;
use Ensi\BuClient\Dto\Store;

class PatchStoreAction
{
    public function __construct(protected readonly StoresApi $storesApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $id, array $fields): Store
    {
        $request = new ExtendedPatchStoreRequest();
        $request->setFields(new PatchStoreRequest($fields));
        $request->setFilter((object)[
            'seller_id' => user()->sellerId,
        ]);

        return $this->storesApi->extendedPatchStore($id, $request)->getData();
    }
}
