<?php

namespace App\Domain\Units\Actions\SellerUsers;

use Ensi\SellerAuthClient\Api\UsersApi;
use Ensi\SellerAuthClient\Dto\ExtendedPatchUserRequest;
use Ensi\SellerAuthClient\Dto\PatchUserRequest;
use Ensi\SellerAuthClient\Dto\User;

class PatchSellerUserAction
{
    public function __construct(protected readonly UsersApi $sellerUsersApi)
    {
    }

    public function execute(int $id, array $fields): User
    {
        $request = new ExtendedPatchUserRequest();
        $request->setFields(new PatchUserRequest($fields));
        $request->setFilter((object)[
            'seller_id' => user()->sellerId,
        ]);

        return $this->sellerUsersApi->extendedPatchUser($id, $request)->getData();
    }
}
