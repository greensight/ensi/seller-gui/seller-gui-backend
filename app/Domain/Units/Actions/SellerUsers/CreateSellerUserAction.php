<?php

namespace App\Domain\Units\Actions\SellerUsers;

use Ensi\SellerAuthClient\Api\UsersApi;
use Ensi\SellerAuthClient\Dto\CreateUserRequest;
use Ensi\SellerAuthClient\Dto\User;

class CreateSellerUserAction
{
    public function __construct(protected readonly UsersApi $sellerUsersApi)
    {
    }

    public function execute(array $fields): User
    {
        $request = new CreateUserRequest($fields);
        $request->setSellerId(user()->sellerId);

        return $this->sellerUsersApi->createUser($request)->getData();
    }
}
