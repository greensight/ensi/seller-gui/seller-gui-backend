<?php

namespace App\Domain\Units\Actions\SellerUsers;

use App\Domain\Common\Actions\Queries\Data\SearchRequestInterface;
use App\Domain\Common\Actions\Queries\SearchQueryAction;
use App\Http\ApiV1\Support\Pagination\Page;
use Ensi\SellerAuthClient\Api\UsersApi;
use Ensi\SellerAuthClient\Dto\SearchUsersRequest;

class SearchSellerUsersAction
{
    public function __construct(
        protected readonly SearchQueryAction $searchQueryAction,
        protected readonly UsersApi $sellerUsersApi,
    ) {
    }

    public function execute(SearchRequestInterface $searchRequest): Page
    {
        return $this->searchQueryAction->execute(
            searchRequest: $searchRequest,
            requestClass: SearchUsersRequest::class,
            search: fn (SearchUsersRequest $request) => $this->sellerUsersApi->searchUsers($request),
            requiredFilter: [
                'seller_id' => user()->sellerId,
            ]
        );
    }
}
