<?php

namespace App\Domain\Units\Actions\SellerUsers;

use App\Domain\Common\Actions\Queries\Data\SearchRequestInterface;
use App\Domain\Common\Actions\Queries\SearchOneQueryAction;
use Ensi\SellerAuthClient\Api\UsersApi;
use Ensi\SellerAuthClient\Dto\SearchUsersRequest;
use Ensi\SellerAuthClient\Dto\User;

class GetSellerUserAction
{
    public function __construct(
        protected readonly SearchOneQueryAction $searchOneQueryAction,
        protected readonly UsersApi $sellerUsersApi,
    ) {
    }

    public function execute(int $id, SearchRequestInterface $searchRequest): User
    {
        return $this->searchOneQueryAction->execute(
            searchRequest: $searchRequest,
            requestClass: SearchUsersRequest::class,
            searchOne: fn (SearchUsersRequest $request) => $this->sellerUsersApi->searchUser($request),
            requiredFilter: [
                'id' => $id,
                'seller_id' => user()->sellerId,
            ]
        );
    }
}
