<?php

namespace App\Domain\Units\Tests\Factories;

use Ensi\BuClient\Dto\SearchStorePickupTimesResponse;
use Ensi\BuClient\Dto\StorePickupTime;
use Ensi\BuClient\Dto\StorePickupTimeResponse;
use Ensi\LaravelTestFactories\BaseApiFactory;

class StorePickupTimeFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'store_id' => $this->faker->modelId(),
            'day' => $this->faker->numberBetween(1, 7),
            'pickup_time_code' => $this->faker->nullable()->exactly(
                "{$this->faker->numberBetween(0, 23)}-{$this->faker->numberBetween(0, 23)}"
            ),
            'pickup_time_start' => $this->faker->time('H:i'),
            'pickup_time_end' => $this->faker->time('H:i'),
            'cargo_export_time' => $this->faker->time('H:i'),
            'delivery_service' => $this->faker->modelId(),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }

    public function make(array $extra = []): StorePickupTime
    {
        return new StorePickupTime($this->makeArray($extra));
    }

    public function makeResponseOne(array $extra = []): StorePickupTimeResponse
    {
        return new StorePickupTimeResponse(['data' => $this->make($extra)]);
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchStorePickupTimesResponse
    {
        return $this->generateResponseSearch(SearchStorePickupTimesResponse::class, $extras, $count, $pagination);
    }
}
