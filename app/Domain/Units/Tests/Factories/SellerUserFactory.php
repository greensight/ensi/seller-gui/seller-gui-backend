<?php

namespace App\Domain\Units\Tests\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\SellerAuthClient\Dto\SearchUsersResponse;
use Ensi\SellerAuthClient\Dto\User;
use Ensi\SellerAuthClient\Dto\UserResponse;

class SellerUserFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'seller_id' => $this->faker->modelId(),
            'login' => $this->faker->unique()->userName(),
            'active' => $this->faker->boolean(),
            'full_name' => $this->faker->name(),
            'short_name' => $this->faker->name(),
            'last_name' => $this->faker->lastName(),
            'first_name' => $this->faker->firstName(),
            'middle_name' => $this->faker->firstName(),
            'phone' => $this->faker->unique()->numerify('+7##########'),
            'email' => $this->faker->unique()->safeEmail(),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }

    public function make(array $extra = []): User
    {
        return new User($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): UserResponse
    {
        return new UserResponse(['data' => $this->make($extra)]);
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchUsersResponse
    {
        return $this->generateResponseSearch(SearchUsersResponse::class, $extras, $count, $pagination);
    }
}
