<?php

namespace App\Domain\Units\Tests\Factories;

use Ensi\BuClient\Dto\SearchSellersResponse;
use Ensi\BuClient\Dto\Seller;
use Ensi\BuClient\Dto\SellerResponse;
use Ensi\BuClient\Dto\SellerStatusEnum;
use Ensi\LaravelTestFactories\BaseApiFactory;

class SellerFactory extends BaseApiFactory
{
    public ?int $storesCount = 0;

    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'manager_id' => $this->faker->nullable()->modelId(),
            'legal_name' => $this->faker->company(),
            'legal_address' => $this->faker->nullable()->exactly(AddressFactory::new()->make()),
            'fact_address' => $this->faker->nullable()->exactly(AddressFactory::new()->make()),
            'inn' => $this->faker->nullable()->numerify('##########'),
            'kpp' => $this->faker->nullable()->numerify('#########'),
            'payment_account' => $this->faker->nullable()->numerify('####################'),
            'correspondent_account' => $this->faker->nullable()->numerify('####################'),
            'bank' => $this->faker->nullable()->company(),
            'bank_address' => $this->faker->nullable()->exactly(AddressFactory::new()->make()),
            'bank_bik' => $this->faker->nullable()->numerify('#########'),
            'status' => $this->faker->randomElement(SellerStatusEnum::getAllowableEnumValues()),
            'status_at' => $this->faker->dateTime(),
            'site' => $this->faker->nullable()->url(),
            'info' => $this->faker->nullable()->text(),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
            'stores' => $this->when(
                $this->storesCount > 0,
                fn () => StoreFactory::new()->makeSeveral($this->storesCount)
            ),
        ];
    }

    public function make(array $extra = []): Seller
    {
        return new Seller($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): SellerResponse
    {
        return new SellerResponse(['data' => $this->make($extra)]);
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchSellersResponse
    {
        return $this->generateResponseSearch(SearchSellersResponse::class, $extras, $count, $pagination);
    }

    public function withStores(int $count): self
    {
        return $this->immutableSet('storesCount', $count);
    }
}
