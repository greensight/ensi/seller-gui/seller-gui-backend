<?php

namespace App\Domain\Units\Tests\Factories;

use Ensi\BuClient\Dto\SearchStoresResponse;
use Ensi\BuClient\Dto\Store;
use Ensi\BuClient\Dto\StoreContact;
use Ensi\BuClient\Dto\StorePickupTime;
use Ensi\BuClient\Dto\StoreResponse;
use Ensi\BuClient\Dto\StoreWorking;
use Ensi\LaravelTestFactories\BaseApiFactory;

class StoreFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'seller_id' => $this->faker->nullable()->modelId(),
            'xml_id' => $this->faker->nullable()->word(),
            'active' => $this->faker->boolean(),
            'name' => $this->faker->name(),
            'address' => $this->faker->nullable()->exactly(AddressFactory::new()->make()),
            'timezone' => $this->faker->timezone(),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }

    public function make(array $extra = []): Store
    {
        return new Store($this->makeArray($extra));
    }

    public function makeResponseOne(array $extra = []): StoreResponse
    {
        return new StoreResponse(['data' => $this->make($extra)]);
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchStoresResponse
    {
        return $this->generateResponseSearch(SearchStoresResponse::class, $extras, $count, $pagination);
    }

    /**
     * @param StoreWorking[]|null $workings
     */
    public function withWorkings(?array $workings = null): static
    {
        return $this->state(['workings' => $workings ?? StoreWorkingFactory::new()->makeSeveral(1)->all()]);
    }

    /**
     * @param StoreContact[]|null $contacts
     */
    public function withContacts(?array $contacts = null): static
    {
        return $this->state(['contacts' => $contacts ?? StoreContactFactory::new()->makeSeveral(1)->all()]);
    }

    public function withContact(?StoreContact $contact = null): static
    {
        return $this->state(['contact' => $contact ?? StoreContactFactory::new()->make()]);
    }

    /**
     * @param StorePickupTime[]|null $pickupTimes
     */
    public function withPickupTimes(?array $pickupTimes = null): static
    {
        return $this->state(['pickup_times' => $pickupTimes ?? StorePickupTimeFactory::new()->makeSeveral(1)->all()]);
    }

    public function withAllRelations(bool $includeAll = true): static
    {
        if ($includeAll) {
            return $this->withWorkings()->withContacts()->withContact()->withPickupTimes();
        }

        return $this;
    }
}
