<?php

namespace App\Domain\Units\Tests\Factories;

use Ensi\BuClient\Dto\SearchStoreContactsResponse;
use Ensi\BuClient\Dto\StoreContact;
use Ensi\BuClient\Dto\StoreContactResponse;
use Ensi\LaravelTestFactories\BaseApiFactory;

class StoreContactFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'store_id' => $this->faker->modelId(),
            'name' => $this->faker->nullable()->name(),
            'phone' => $this->faker->nullable()->numerify('+7##########'),
            'email' => $this->faker->nullable()->email(),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }

    public function make(array $extra = []): StoreContact
    {
        return new StoreContact($this->makeArray($extra));
    }

    public function makeResponseOne(array $extra = []): StoreContactResponse
    {
        return new StoreContactResponse(['data' => $this->make($extra)]);
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchStoreContactsResponse
    {
        return $this->generateResponseSearch(SearchStoreContactsResponse::class, $extras, $count, $pagination);
    }
}
