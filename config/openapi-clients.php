<?php

return [
    'units' => [
        'seller-auth' => [
            'base_uri' => env('UNITS_SELLER_AUTH_SERVICE_HOST') . "/api/v1",
            'client' => [
                'id' => env('UNITS_SELLER_AUTH_SERVICE_CLIENT_ID', ''),
                'secret' => env('UNITS_SELLER_AUTH_SERVICE_CLIENT_SECRET', ''),
            ],
        ],
        'bu' => [
            'base_uri' => env('UNITS_BU_SERVICE_HOST') . "/api/v1",
        ],
    ],
    'logistic' => [
        'logistic' => [
            'base_uri' => env('LOGISTIC_LOGISTIC_SERVICE_HOST') . "/api/v1",
        ],
    ],
];
