<?php

namespace Tests;

use Tests\MockTraits\MockBuServicesApi;
use Tests\MockTraits\MockLogisticServicesApi;
use Tests\MockTraits\MockSellerAuthServicesApi;

trait MockServicesApi
{
    use MockSellerAuthServicesApi;
    use MockBuServicesApi;
    use MockLogisticServicesApi;
}
