<?php

namespace Tests\MockTraits;

use Ensi\SellerAuthClient\Api\OauthApi;
use Ensi\SellerAuthClient\Api\UsersApi as SellerAuthUsersApi;
use Ensi\SellerAuthClient\Configuration;
use Mockery\MockInterface;

trait MockSellerAuthServicesApi
{
    public function mockSellerAuthOauthApi(): OauthApi|MockInterface
    {
        return $this->mock(OauthApi::class)->allows([
            'getConfig' => resolve(Configuration::class),
        ]);
    }

    public function mockSellerAuthUsersApi(): MockInterface|Configuration
    {
        return $this->mock(SellerAuthUsersApi::class)->allows([
            'getConfig' => resolve(Configuration::class),
        ]);
    }
}
