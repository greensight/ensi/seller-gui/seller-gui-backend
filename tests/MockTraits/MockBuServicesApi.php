<?php

namespace Tests\MockTraits;

use Ensi\BuClient\Api\SellersApi;
use Ensi\BuClient\Api\StoreContactsApi;
use Ensi\BuClient\Api\StorePickupTimesApi;
use Ensi\BuClient\Api\StoresApi;
use Ensi\BuClient\Api\StoreWorkingsApi;
use Mockery\MockInterface;

trait MockBuServicesApi
{
    public function mockBusinessUnitsSellersApi(): MockInterface|SellersApi
    {
        return $this->mock(SellersApi::class);
    }

    public function mockBusinessUnitsStoresApi(): MockInterface|StoresApi
    {
        return $this->mock(StoresApi::class);
    }

    public function mockBusinessUnitsStoreContactsApi(): MockInterface|StoreContactsApi
    {
        return $this->mock(StoreContactsApi::class);
    }

    public function mockBusinessUnitsStoreWorkingsApi(): MockInterface|StoreWorkingsApi
    {
        return $this->mock(StoreWorkingsApi::class);
    }

    public function mockBusinessUnitsStorePickupTimesApi(): MockInterface|StorePickupTimesApi
    {
        return $this->mock(StorePickupTimesApi::class);
    }
}
