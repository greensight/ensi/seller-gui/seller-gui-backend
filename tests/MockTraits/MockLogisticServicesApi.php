<?php

namespace Tests\MockTraits;

use Ensi\LogisticClient\Api\DeliveryServicesApi;
use Mockery\MockInterface;

trait MockLogisticServicesApi
{
    public function mockLogisticDeliveryServicesApi(): MockInterface|DeliveryServicesApi
    {
        return $this->mock(DeliveryServicesApi::class);
    }
}
