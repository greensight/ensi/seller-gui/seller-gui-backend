# Seller GUI Backend

## Резюме

Название: Seller GUI Backend  
Домен: Seller GUI  
Назначение: Backend для административного интерфейса продавцов

## Разработка сервиса

Инструкцию описывающую разворот, запуск и тестирование сервиса на локальной машине можно найти в отдельном документе в [Gitlab Pages](https://ensi-platform.gitlab.io/docs/tech/back)

Регламент работы над задачами тоже находится в [Gitlab Pages](https://ensi-platform.gitlab.io/docs/guid/regulations)

### Настройка авторизации

Для настройки авторизации пользователей в .env файл необходимо добавить переменные *UNITS_SELLER_AUTH_SERVICE_CLIENT_ID* и *UNITS_SELLER_AUTH_SERVICE_CLIENT_SECRET*. Значения данных переменных могут быть получены:
1. Из консоли при создании клиента в соответствующем *-auth сервисе (подробнее про создание клиента см. readme соответствующего *-auth сервиса)

2. Если клиент уже сгенерирован, то в таблице *oauth_clients* в соответствующем *-auth сервисе

## Структура сервиса

Почитать про структуру сервиса можно [здесь](https://docs.ensi.tech/backend-guides/principles/service-structure)

## Зависимости

| Название | Описание                                                 | Переменные окружения |
| --- |----------------------------------------------------------| --- |
| **Сервисы Ensi** | **Сервисы Ensi, с которыми данный сервис коммуницирует** |


## Среды

### Test

CI: https://jenkins-infra.ensi.tech/job/ensi-stage-1/job/seller-gui/job/seller-gui-backend/  
URL: https://seller-gui-backend-master-dev.ensi.tech/docs/swagger

### Preprod

Отсутствует

### Prod

Отсутствует

## Контакты

Команда поддерживающая данный сервис: https://gitlab.com/groups/greensight/ensi/-/group_members  
Email для связи: mail@greensight.ru

## Лицензия

[Открытая лицензия на право использования программы для ЭВМ Greensight Ecom Platform (GEP)](LICENSE.md).
