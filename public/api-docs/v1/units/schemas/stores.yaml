StoreReadonlyProperties:
  type: object
  properties:
    id:
      type: integer
      description: Идентификатор склада
      example: 1
    seller_id:
      type: integer
      description: ID продавца
      nullable: true
      example: 2
    created_at:
      type: string
      format: date-time
      description: Время создания склада
      example: "2021-01-15T14:55:35.000000Z"
    updated_at:
      type: string
      format: date-time
      description: Время обновления склада
      example: "2021-01-15T14:55:35.000000Z"
  required:
    - id
    - seller_id
    - created_at
    - updated_at

StoreFillableProperties:
  type: object
  properties:
    xml_id:
      nullable: true
      type: string
      description: ID склада у продавца
      example: "015"
    active:
      type: boolean
      description: Флаг активности склада
      example: true
    name:
      type: string
      description: Название
      example: "Сокольники"
    address:
      $ref: './data/address.yaml#/Address'
    timezone:
      type: string
      description: Часовой пояс
      example: "Europe/Moscow"


StoreFillableRequiredProperties:
  required:
    - xml_id
    - active
    - name
    - timezone

StoreIncludes:
  type: object
  properties:
    workings:
      type: array
      items:
        $ref: './store_workings.yaml#/StoreWorking'
    contacts:
      type: array
      items:
        $ref: './store_contacts.yaml#/StoreContact'
    contact:
      $ref: './store_contacts.yaml#/StoreContact'
    pickup_times:
      type: array
      items:
        $ref: './store_pickup_times.yaml#/StorePickupTime'

Store:
  allOf:
    - $ref: '#/StoreReadonlyProperties'
    - $ref: '#/StoreFillableProperties'
    - $ref: '#/StoreFillableRequiredProperties'
    - $ref: '#/StoreIncludes'

CreateStoreRequest:
  allOf:
    - $ref: '#/StoreFillableProperties'
    - $ref: '#/StoreFillableRequiredProperties'

PatchStoreRequest:
  allOf:
    - $ref: '#/StoreFillableProperties'


SearchStoresRequest:
  type: object
  properties:
    sort:
      description: "Сортировка. Доступные для сортировки поля см. в :meta"
      $ref: '../../common_schemas.yaml#/RequestBodySort'
    filter:
      type: object
      description: "Фильтр. Доступные для фильтрации поля см. в :meta"
    include:
      type: array
      items:
        type: string
        enum:
          - workings
          - contacts
          - contact
          - pickup_times
    pagination:
      $ref: '../../common_schemas.yaml#/RequestBodyPagination'

StoreResponse:
  type: object
  properties:
    data:
      $ref: '#/Store'
    meta:
      type: object
  required:
    - data

SearchStoresResponse:
  type: object
  properties:
    data:
      type: array
      items:
        $ref: '#/Store'
    meta:
      type: object
      properties:
        pagination:
          $ref: '../../common_schemas.yaml#/ResponseBodyPagination'
  required:
    - data
    - meta
